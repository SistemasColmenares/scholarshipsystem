<?php

namespace Tests\Feature;

use App\Models\Admin\Student\Student;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Artisan;
use Tests\TestCase;

class StudentTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected $student;

    protected function setUp(): void
    {
        parent::setUp();
        Artisan::call('migrate');
        Artisan::call('db:seed');
        $this->student = Student::first();
    }

    /** @test */
    public function can_update_students()
    {
        $this->put('/api/auth/student/1', [
            'register' => $this->faker->randomNumber(),
            'family_id' => $this->faker->randomNumber(),
            'lastname' => $this->faker->lastname,
            'second_lastname' => $this->faker->lastname,
            'name' => $this->faker->name,
            'grade' => $this->faker->randomDigit,
            'group' => $this->faker->randomLetter,
            'section' => $this->faker->randomFloat(1, 0, 3),
            'letter' => $this->faker->randomLetter,
            'entry_status' => $this->faker->randomLetter,
            'la_status' => $this->faker->numerify('F###'),
        ])->assertStatus(200);

    }

    /** @test */
    public function can_read_students()
    {
        $this->get('/api/auth/student', [
        ])->assertStatus(200);

    }

    /** @test */
    public function can_read_a_student()
    {
        $this->get('/api/auth/student/1', [
        ])->assertStatus(200);

    }
}
