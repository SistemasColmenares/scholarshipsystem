<?php

namespace Tests\Feature;

use App\Models\Admin\Settings\Settings;
use App\Models\User\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Artisan;
use Tests\TestCase;

class SettingsTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected $settings;

    protected function setUp(): void
    {
        parent::setUp();
        Artisan::call('migrate');
        Artisan::call('db:seed');
        $this->user = User::first();
        $this->settings = Settings::first();
    }

    /** @test */
    public function can_read_settings()
    {
        $this->actingAs($this->user)->get('/api/auth/settings')
            ->assertStatus(200);
    }

    /** @test */
    public function can_update_settings()
    {
        $this->put('/api/auth/settings/'.$this->settings->id, [
            'start_date' => $this->faker->date(),
            'end_date' => $this->faker->date(),
            'research_cost' => $this->faker->randomFloat(2, 0, 1000),
            'inscription_percent' => $this->faker->randomFloat(2, 0, 100),
            'three_children_percent' => $this->faker->randomFloat(2, 0, 100),
            'four_up_children_percent' => $this->faker->randomFloat(2, 0, 100),
        ])->assertStatus(200);
    }
}
