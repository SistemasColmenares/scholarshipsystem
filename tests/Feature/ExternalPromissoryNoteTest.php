<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Artisan;
use Tests\TestCase;

class ExternalPromissoryNoteTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();
        Artisan::call('migrate');
        Artisan::call('db:seed');

    }

    /** @test */
    public function can_read_externañ_promissory_notes()
    {
        $this->get('/api/auth/externalPromissoryNotes', [
        ])->assertStatus(200);

    }
}
