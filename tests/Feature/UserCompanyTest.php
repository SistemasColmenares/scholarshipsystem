<?php

namespace Tests\Feature;

use App\Models\Admin\UserCompany\UserCompany;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Artisan;
use Tests\TestCase;

class UserCompanyTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected $user;

    protected function setUp(): void
    {
        parent::setUp();
        Artisan::call('migrate');
        Artisan::call('db:seed');
        $this->user = UserCompany::first();
    }

    /** @test */
    public function can_create_user_companies()
    {
        $this->post('/api/auth/registerCompany/', [
            'name' => 'Angel',
            'email' => 'user@example.com',
            'password' => 'password',
            'password_confirmation' => 'password',
        ])->assertStatus(201);
    }

    /** @test */
    public function can_update_user_companies()
    {
        $response = $this->put('/api/auth/userCompany/1', [
            'name' => 'Angel',
            'email' => 'julian.lopez@colmenares.org.mx',
            'password' => 'password',
            'password_confirmation' => 'password',
        ])->assertStatus(200);
    }

    /** @test */
    public function can_read_user_companies()
    {
        $this->get('/api/auth/userCompany', [
        ])->assertStatus(200);

    }

    /** @test */
    public function can_read_a_user_company()
    {
        $this->get('/api/auth/userCompany/1', [
        ])->assertStatus(200);

    }
}
