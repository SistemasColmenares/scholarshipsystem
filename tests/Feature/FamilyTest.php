<?php

namespace Tests\Feature;

use App\Models\Admin\Family\Family;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Artisan;
use Tests\TestCase;

class FamilyTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected $family;

    protected function setUp(): void
    {
        parent::setUp();
        Artisan::call('migrate');
        Artisan::call('db:seed');
        $this->family = Family::first();
    }

    /** @test */
    public function can_update_families()
    {
        $this->put('/api/auth/family/1', [
            'address' => $this->faker->address,
            'pc' => $this->faker->randomNumber(),
            'suburb' => $this->faker->streetName,
            'phone_number' => 3313118745,
            'ft_lastname' => $this->faker->lastname,
            'ft_second_lastname' => $this->faker->lastname,
            'ft_name' => $this->faker->name,
            'ft_birthdate' => $this->faker->date,
            'ft_email' => $this->faker->email,
            'ft_business_name' => $this->faker->company,
            'ft_business_activity' => $this->faker->randomLetter,
            'ft_business_position' => $this->faker->jobTitle,
            'ft_business_society' => $this->faker->randomLetter,
            'ft_business_percent' => $this->faker->randomFloat(1, 0, 100),
            'mt_lastname' => $this->faker->lastname,
            'mt_second_lastname' => $this->faker->lastname,
            'mt_name' => $this->faker->name,
            'mt_birthdate' => $this->faker->date,
            'mt_email' => $this->faker->email,
            'mt_business_name' => $this->faker->company,
            'mt_business_activity' => $this->faker->randomLetter,
            'mt_business_position' => $this->faker->jobTitle,
            'mt_business_society' => $this->faker->randomLetter,
            'mt_business_percent' => $this->faker->randomFloat(1, 0, 100),
        ])->assertStatus(200);

    }

    /** @test */
    public function can_read_families()
    {
        $this->get('/api/auth/family', [
        ])->assertStatus(200);

    }

    /** @test */
    public function can_read_a_family()
    {
        $this->get('/api/auth/family/1', [
        ])->assertStatus(200);

    }
}
