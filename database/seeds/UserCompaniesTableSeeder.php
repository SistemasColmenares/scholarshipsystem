<?php

use App\Models\Admin\UserCompany\UserCompany;
use Illuminate\Database\Seeder;

class UserCompaniesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        UserCompany::create([
            'name' => 'Company',
            'email' => 'company@company.com',
            'password' => Hash::make('company'),
            'image' => 'https://placeholder.com/300x600',
            'image_name' => 'company',

        ]);
        factory(UserCompany::class, 10)->create();
    }
}
