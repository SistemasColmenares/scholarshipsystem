<?php

use App\Models\Admin\Family\Family;
use Faker\Generator as Faker;
use Illuminate\Database\Seeder;

class FamiliesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(Faker $faker)
    {
        Family::create([
            'family_id' => 11677,
            'address' => $faker->address,
            'town' => $faker->city,
            'pc' => $faker->randomNumber(),
            'suburb' => $faker->streetName,
            'phone_number' => $faker->numerify('##########'),
            'ft_lastname' => $faker->lastname,
            'ft_second_lastname' => $faker->lastname,
            'ft_name' => $faker->name,
            'ft_birthdate' => $faker->date,
            'ft_email' => $faker->email,
            'ft_cellphone_number' => $faker->numerify('##########'),
            'ft_business_name' => $faker->company,
            'ft_business_activity' => $faker->randomLetter,
            'ft_business_position' => $faker->jobTitle,
            'ft_business_antiquity' => $faker->randomDigit(),
            'ft_business_society' => $faker->randomLetter,
            'ft_business_percent' => $faker->randomFloat(1, 0, 100),
            'mt_lastname' => $faker->lastname,
            'mt_second_lastname' => $faker->lastname,
            'mt_name' => $faker->name,
            'mt_birthdate' => $faker->date,
            'mt_email' => $faker->email,
            'mt_cellphone_number' => $faker->numerify('##########'),
            'mt_business_name' => $faker->company,
            'mt_business_activity' => $faker->randomLetter,
            'mt_business_position' => $faker->jobTitle,
            'mt_business_antiquity' => $faker->randomDigit(),
            'mt_business_society' => $faker->randomLetter,
            'mt_business_percent' => $faker->randomFloat(1, 0, 100),
        ]);
        //factory(Family::class, 200)->create();
    }
}
