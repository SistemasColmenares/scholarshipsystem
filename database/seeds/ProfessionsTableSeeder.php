<?php

use App\Models\GeneralResources\Profession\Profession;
use Illuminate\Database\Seeder;

class ProfessionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Profession::create([
            'name' => 'Ninguna',
            'abbreviation' => 'CERO',
        ]);
        Profession::create([
            'name' => 'Abogado',
            'abbreviation' => 'ABOG',
        ]);
        Profession::create([
            'name' => 'Ama de casa',
            'abbreviation' => 'AC',
        ]);
        Profession::create([
            'name' => 'Actuario',
            'abbreviation' => 'ACT',
        ]);
        Profession::create([
            'name' => 'Administrador',
            'abbreviation' => 'ADM',
        ]);
        Profession::create([
            'name' => 'Asesor financiero',
            'abbreviation' => 'AF',
        ]);
        Profession::create([
            'name' => 'Afanador',
            'abbreviation' => 'AFAN',
        ]);
        Profession::create([
            'name' => 'Agricultor',
            'abbreviation' => 'AG',
        ]);
        Profession::create([
            'name' => 'Agente federal',
            'abbreviation' => 'AGF',
        ]);
        Profession::create([
            'name' => 'Agronomo',
            'abbreviation' => 'AGRO',
        ]);
        Profession::create([
            'name' => 'Arquitecto',
            'abbreviation' => 'ARQ',
        ]);
        Profession::create([
            'name' => 'Artista',
            'abbreviation' => 'ART',
        ]);
        Profession::create([
            'name' => 'Asesor inmobiliario',
            'abbreviation' => 'ASEI',
        ]);
        Profession::create([
            'name' => 'Auxiliar contable',
            'abbreviation' => 'AUX',
        ]);
        Profession::create([
            'name' => 'Avicultor',
            'abbreviation' => 'AVI',
        ]);
        Profession::create([
            'name' => 'Bibliotecario',
            'abbreviation' => 'BIBL',
        ]);
        Profession::create([
            'name' => 'Bombero',
            'abbreviation' => 'BOMB',
        ]);
        Profession::create([
            'name' => 'Capturista',
            'abbreviation' => 'CAP',
        ]);
        Profession::create([
            'name' => 'Contador privado',
            'abbreviation' => 'CC',
        ]);
        Profession::create([
            'name' => 'Consultor familiar',
            'abbreviation' => 'CFAM',
        ]);
        Profession::create([
            'name' => 'Cajero',
            'abbreviation' => 'CJ',
        ]);
        Profession::create([
            'name' => 'Comerciante',
            'abbreviation' => 'COM',
        ]);
        Profession::create([
            'name' => 'Ciencias computacionales',
            'abbreviation' => 'COMP',
        ]);
        Profession::create([
            'name' => 'Contratista',
            'abbreviation' => 'CON¿',
        ]);
        Profession::create([
            'name' => 'Consultor general',
            'abbreviation' => 'CONG',
        ]);
        Profession::create([
            'name' => 'Construcción',
            'abbreviation' => 'CONS',
        ]);
        Profession::create([
            'name' => 'Cosmetologo',
            'abbreviation' => 'COSM',
        ]);
        Profession::create([
            'name' => 'Contador público',
            'abbreviation' => 'CP',
        ]);
        Profession::create([
            'name' => 'Carrera trunca',
            'abbreviation' => 'CT',
        ]);
        Profession::create([
            'name' => 'Chofer',
            'abbreviation' => 'CHOF',
        ]);
        Profession::create([
            'name' => 'Decorador',
            'abbreviation' => 'DECO',
        ]);
        Profession::create([
            'name' => 'Diseño gráfico',
            'abbreviation' => 'DG',
        ]);
        Profession::create([
            'name' => 'Cirujano dentista',
            'abbreviation' => 'DENT',
        ]);
        Profession::create([
            'name' => 'Diseñador gráfico',
            'abbreviation' => 'DG',
        ]);
        Profession::create([
            'name' => 'Diseñador de interiores',
            'abbreviation' => 'DIS',
        ]);
        Profession::create([
            'name' => 'Diseñador industrial',
            'abbreviation' => 'DISE',
        ]);
        Profession::create([
            'name' => 'Diseñador de modas',
            'abbreviation' => 'DM',
        ]);
        Profession::create([
            'name' => 'Diseño publicitario',
            'abbreviation' => 'DP',
        ]);
        Profession::create([
            'name' => 'Ciencias del desarrollo humano',
            'abbreviation' => 'DR',
        ]);
        Profession::create([
            'name' => 'Director de sucursal bancaria',
            'abbreviation' => 'DSB',
        ]);
        Profession::create([
            'name' => 'Director técnico',
            'abbreviation' => 'DT',
        ]);
        Profession::create([
            'name' => 'Economista',
            'abbreviation' => 'ECON',
        ]);
        Profession::create([
            'name' => 'Educacador técnico',
            'abbreviation' => 'EDT',
        ]);
        Profession::create([
            'name' => 'Educador',
            'abbreviation' => 'ED',
        ]);
        Profession::create([
            'name' => 'Educación física',
            'abbreviation' => 'EF',
        ]);
        Profession::create([
            'name' => 'Empresario',
            'abbreviation' => 'EMPR',
        ]);
        Profession::create([
            'name' => 'Enfermero',
            'abbreviation' => 'ENF',
        ]);
        Profession::create([
            'name' => 'Estudiante',
            'abbreviation' => 'EST',
        ]);
        Profession::create([
            'name' => 'Docente de inglés',
            'abbreviation' => 'EXT',
        ]);
        Profession::create([
            'name' => 'Físico matemático',
            'abbreviation' => 'FM',
        ]);
        Profession::create([
            'name' => 'Fotógrafo',
            'abbreviation' => 'FOTO',
        ]);
        Profession::create([
            'name' => 'Fruticultor',
            'abbreviation' => 'FRUT',
        ]);
        Profession::create([
            'name' => 'Futbolista profesional',
            'abbreviation' => 'FUT',
        ]);
        Profession::create([
            'name' => 'Ganadero',
            'abbreviation' => 'GAN',
        ]);
        Profession::create([
            'name' => 'Ginecólogo',
            'abbreviation' => 'GINE',
        ]);
        Profession::create([
            'name' => 'Hogar',
            'abbreviation' => 'HOG',
        ]);
        Profession::create([
            'name' => 'Ing. Agrónomo',
            'abbreviation' => 'IAGR',
        ]);
        Profession::create([
            'name' => 'Ing.Bioquímico',
            'abbreviation' => 'IBIO',
        ]);
        Profession::create([
            'name' => 'Ing. en comunicaciones y electrónica',
            'abbreviation' => 'ICE',
        ]);
        Profession::create([
            'name' => 'Ing. Civil',
            'abbreviation' => 'ICIV',
        ]);
        Profession::create([
            'name' => 'Ing. en computación',
            'abbreviation' => 'ICOM',
        ]);
        Profession::create([
            'name' => 'Idiomas',
            'abbreviation' => 'IDI',
        ]);
        Profession::create([
            'name' => 'Ing. Electricista',
            'abbreviation' => 'IELEC',
        ]);
        Profession::create([
            'name' => 'Ing. Electrónico',
            'abbreviation' => 'IELE',
        ]);
        Profession::create([
            'name' => 'Ing. Geólogo',
            'abbreviation' => 'IGEO',
        ]);
        Profession::create([
            'name' => 'Ing. Mecánico',
            'abbreviation' => 'IMEC',
        ]);
        Profession::create([
            'name' => 'Ing. en Minas',
            'abbreviation' => 'IMIN',
        ]);
        Profession::create([
            'name' => 'Ing. Industrial',
            'abbreviation' => 'IND',
        ]);
        Profession::create([
            'name' => 'Industrial',
            'abbreviation' => 'INDU',
        ]);
        Profession::create([
            'name' => 'Ingeniero',
            'abbreviation' => 'ING',
        ]);
        Profession::create([
            'name' => 'Ing. Químico',
            'abbreviation' => 'IQ',
        ]);
        Profession::create([
            'name' => 'Ing. en Sistemas',
            'abbreviation' => 'ISIS',
        ]);
        Profession::create([
            'name' => 'Ing. Textil',
            'abbreviation' => 'IT',
        ]);
        Profession::create([
            'name' => 'Ing. Topógrafo',
            'abbreviation' => 'ITOP',
        ]);
        Profession::create([
            'name' => 'Intendente',
            'abbreviation' => 'INT',
        ]);
        Profession::create([
            'name' => 'Jardinero',
            'abbreviation' => 'JAR',
        ]);
        Profession::create([
            'name' => 'Laborista',
            'abbreviation' => 'LAB',
        ]);
        Profession::create([
            'name' => 'Lic. en administración de empresas',
            'abbreviation' => 'LAE',
        ]);
        Profession::create([
            'name' => 'Lic. Administración y finanzas',
            'abbreviation' => 'LAF',
        ]);
        Profession::create([
            'name' => 'Lic. Administración pública',
            'abbreviation' => 'LAP',
        ]);
        Profession::create([
            'name' => 'Lic. en Biología',
            'abbreviation' => 'LBIO',
        ]);
        Profession::create([
            'name' => 'Lic. Ciencias de comunicación',
            'abbreviation' => 'LCC',
        ]);
        Profession::create([
            'name' => 'Lic. en Deports',
            'abbreviation' => 'LD',
        ]);
        Profession::create([
            'name' => 'Lic. en Diseño',
            'abbreviation' => 'LDIS',
        ]);
        Profession::create([
            'name' => 'Lic. en Educación',
            'abbreviation' => 'LEDU',
        ]);
        Profession::create([
            'name' => 'Lic. en Educación física',
            'abbreviation' => 'LEF',
        ]);
        Profession::create([
            'name' => 'Lic. en Educación preescolar',
            'abbreviation' => 'LEP',
        ]);
        Profession::create([
            'name' => 'Lic. en Filosofía',
            'abbreviation' => 'LEFIL',
        ]);
        Profession::create([
            'name' => 'Lic. en Humanidades',
            'abbreviation' => 'LHUM',
        ]);
        Profession::create([
            'name' => 'Licenciado',
            'abbreviation' => 'LI',
        ]);
        Profession::create([
            'name' => 'Lic. Informática administrativa',
            'abbreviation' => 'LIA',
        ]);
        Profession::create([
            'name' => 'Lic. en Derecho',
            'abbreviation' => 'LIC',
        ]);
        Profession::create([
            'name' => 'Lic. en Literatura',
            'abbreviation' => 'LICL',
        ]);
        Profession::create([
            'name' => 'Lic. en Matemáticas',
            'abbreviation' => 'LICM',
        ]);
        Profession::create([
            'name' => 'Lic. en Informática',
            'abbreviation' => 'LINF',
        ]);
        Profession::create([
            'name' => 'Lic. Comercio internacional',
            'abbreviation' => 'LINT',
        ]);
        Profession::create([
            'name' => 'Lic. Mercadotecnia',
            'abbreviation' => 'LMER',
        ]);
        Profession::create([
            'name' => 'Lic. Psicología',
            'abbreviation' => 'LPSI',
        ]);
        Profession::create([
            'name' => 'Lic. Relaciones industriales',
            'abbreviation' => 'LRI',
        ]);
        Profession::create([
            'name' => 'Lic. en Sistemas',
            'abbreviation' => 'LSIS',
        ]);
        Profession::create([
            'name' => 'Lic. en Turismo',
            'abbreviation' => 'LTUR',
        ]);
        Profession::create([
            'name' => 'Lic. en Letras',
            'abbreviation' => 'LL',
        ]);
        Profession::create([
            'name' => 'Médico estomatólogo',
            'abbreviation' => 'ME',
        ]);
        Profession::create([
            'name' => 'Médico',
            'abbreviation' => 'MED',
        ]);
        Profession::create([
            'name' => 'Militar',
            'abbreviation' => 'MIL',
        ]);
        Profession::create([
            'name' => 'Médico pediatra',
            'abbreviation' => 'MPED',
        ]);
        Profession::create([
            'name' => 'Músico',
            'abbreviation' => 'MUS',
        ]);
        Profession::create([
            'name' => 'Normalista',
            'abbreviation' => 'NOR',
        ]);
        Profession::create([
            'name' => 'Nutriólogp',
            'abbreviation' => 'NUT',
        ]);
        Profession::create([
            'name' => 'Oftalmólogo',
            'abbreviation' => 'OFTA',
        ]);
        Profession::create([
            'name' => 'Optometrista',
            'abbreviation' => 'OPT',
        ]);
        Profession::create([
            'name' => 'Orientador familiar',
            'abbreviation' => 'ORF',
        ]);
        Profession::create([
            'name' => 'Paramédico',
            'abbreviation' => 'PARM',
        ]);
        Profession::create([
            'name' => 'Pedagógo',
            'abbreviation' => 'PED',
        ]);
        Profession::create([
            'name' => 'Piloto aviador',
            'abbreviation' => 'PIL',
        ]);
        Profession::create([
            'name' => 'Cirujano plástico',
            'abbreviation' => 'PLAS',
        ]);
        Profession::create([
            'name' => 'Productor musical',
            'abbreviation' => 'PMUS',
        ]);
        Profession::create([
            'name' => 'Profesor normalista',
            'abbreviation' => 'PN',
        ]);
        Profession::create([
            'name' => 'Profesor',
            'abbreviation' => 'PROF',
        ]);
        Profession::create([
            'name' => 'Paquetería',
            'abbreviation' => 'PQT',
        ]);
        Profession::create([
            'name' => 'Puericultura',
            'abbreviation' => 'PUE',
        ]);
        Profession::create([
            'name' => 'Químico farmaco-biólogo',
            'abbreviation' => 'QFB',
        ]);
        Profession::create([
            'name' => 'Recepcionista',
            'abbreviation' => 'REC',
        ]);
        Profession::create([
            'name' => 'Relaciones púbilcas',
            'abbreviation' => 'RP',
        ]);
        Profession::create([
            'name' => 'Restaurantero',
            'abbreviation' => 'REST',
        ]);
        Profession::create([
            'name' => 'Secretaria',
            'abbreviation' => 'SECR',
        ]);
        Profession::create([
            'name' => 'Seguros',
            'abbreviation' => 'SG',
        ]);
        Profession::create([
            'name' => 'Sin profesión',
            'abbreviation' => 'SIN',
        ]);
        Profession::create([
            'name' => 'Señor',
            'abbreviation' => 'SR',
        ]);
        Profession::create([
            'name' => 'Señora',
            'abbreviation' => 'SRA',
        ]);
        Profession::create([
            'name' => 'Señorita',
            'abbreviation' => 'SRITA',
        ]);
        Profession::create([
            'name' => 'Taxista',
            'abbreviation' => 'TAX',
        ]);
        Profession::create([
            'name' => 'Tec. en computación',
            'abbreviation' => 'TCOM',
        ]);
        Profession::create([
            'name' => 'Técnico',
            'abbreviation' => 'TEC',
        ]);
        Profession::create([
            'name' => 'Tiendas',
            'abbreviation' => 'TDS',
        ]);
        Profession::create([
            'name' => 'Tec. Electricista',
            'abbreviation' => 'TECE',
        ]);
        Profession::create([
            'name' => 'Tec.en química',
            'abbreviation' => 'TECQ',
        ]);
        Profession::create([
            'name' => 'Tec.en educación',
            'abbreviation' => 'TED',
        ]);
        Profession::create([
            'name' => 'Tec. Laborista',
            'abbreviation' => 'TLAB',
        ]);
        Profession::create([
            'name' => 'Tec. Mecánico',
            'abbreviation' => 'TM',
        ]);
        Profession::create([
            'name' => 'Terapeuta de lenguaje',
            'abbreviation' => 'TERL',
        ]);
        Profession::create([
            'name' => 'Terapeuta',
            'abbreviation' => 'TER',
        ]);
        Profession::create([
            'name' => 'Trailero',
            'abbreviation' => 'TR',
        ]);
        Profession::create([
            'name' => 'Trabajador social',
            'abbreviation' => 'TS',
        ]);
        Profession::create([
            'name' => 'Viudo',
            'abbreviation' => 'VDA',
        ]);
        Profession::create([
            'name' => 'Veterinario',
            'abbreviation' => 'VETE',
        ]);
    }
}
