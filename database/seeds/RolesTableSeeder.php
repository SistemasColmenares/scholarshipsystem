<?php

use App\Models\User\Role\Role;
use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::create([
            'name' => 'SuperAdministrador',
        ]);
        Role::create([
            'name' => 'Administrador',
        ]);
        Role::create([
            'name' => 'Colegio',
        ]);
        Role::create([
            'name' => 'Colegio-Asistente',
        ]);
    }
}
