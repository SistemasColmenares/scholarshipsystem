<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(RolesTableSeeder::class);
        $this->call(PermissionsTableSeeder::class);
        $this->call(StudentsTableSeeder::class);
        $this->call(SettingsTableSeeder::class);
        $this->call(FamiliesTableSeeder::class);
        $this->call(UserCompaniesTableSeeder::class);
        $this->call(ExternalPromissoryNotesTableSeeder::class);
        $this->call(UserFamiliesTableSeeder::class);
        $this->call(UpStudentsTableSeeder::class);
        $this->call(CampusesTableSeeder::class);
        $this->call(UpStatusesTableSeeder::class);
        $this->call(UpCareersTableSeeder::class);
        $this->call(BusinessActivitiesTableSeeder::class);
        $this->call(SchoolarshipsTableSeeder::class);
        $this->call(ProfessionsTableSeeder::class);
    }
}
