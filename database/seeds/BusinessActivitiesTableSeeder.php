<?php

use App\Models\GeneralResources\BusinessActivity\BusinessActivity;
use Illuminate\Database\Seeder;

class BusinessActivitiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        BusinessActivity::create([
            'name' => 'Abogado',
            'abbreviation' => 'ABO',
        ]);
        BusinessActivity::create([
            'name' => 'Administración',
            'abbreviation' => 'ADM',
        ]);
        BusinessActivity::create([
            'name' => 'Agropecuaria',
            'abbreviation' => 'AGR',
        ]);
        BusinessActivity::create([
            'name' => 'Agua y refrescos',
            'abbreviation' => 'AGU',
        ]);
        BusinessActivity::create([
            'name' => 'Alimentos',
            'abbreviation' => 'ALI',
        ]);
        BusinessActivity::create([
            'name' => 'Artesanal',
            'abbreviation' => 'ART',
        ]);
        BusinessActivity::create([
            'name' => 'Asesoria y consultoria',
            'abbreviation' => 'ASE',
        ]);
        BusinessActivity::create([
            'name' => 'Asistencia social',
            'abbreviation' => 'ASI',
        ]);
        BusinessActivity::create([
            'name' => 'Automotriz',
            'abbreviation' => '',
        ]);
        BusinessActivity::create([
            'name' => 'Medicina / Hospitales',
            'abbreviation' => 'AV',
        ]);
        BusinessActivity::create([
            'name' => 'Avicola',
            'abbreviation' => 'AVI',
        ]);
        BusinessActivity::create([
            'name' => 'BANCA',
            'abbreviation' => 'BANC',
        ]);
        BusinessActivity::create([
            'name' => 'Bienes raíces',
            'abbreviation' => 'BR',
        ]);
        BusinessActivity::create([
            'name' => 'Carpinteria',
            'abbreviation' => 'CAR',
        ]);
        BusinessActivity::create([
            'name' => 'Comunicación',
            'abbreviation' => 'CMN',
        ]);
        BusinessActivity::create([
            'name' => 'Computación',
            'abbreviation' => 'CMP',
        ]);
        BusinessActivity::create([
            'name' => 'Contable',
            'abbreviation' => 'CNT',
        ]);
        BusinessActivity::create([
            'name' => 'Comercial',
            'abbreviation' => 'COM',
        ]);
        BusinessActivity::create([
            'name' => 'Construcción',
            'abbreviation' => 'CON',
        ]);
        BusinessActivity::create([
            'name' => 'Por definir',
            'abbreviation' => 'DEF',
        ]);
        BusinessActivity::create([
            'name' => 'Deports',
            'abbreviation' => 'DEP',
        ]);
        BusinessActivity::create([
            'name' => 'Diseño y artes gráficas',
            'abbreviation' => 'DIS',
        ]);
        BusinessActivity::create([
            'name' => 'Educación',
            'abbreviation' => 'EDU',
        ]);
        BusinessActivity::create([
            'name' => 'Electrónica',
            'abbreviation' => 'ELE',
        ]);
        BusinessActivity::create([
            'name' => 'Gobierno',
            'abbreviation' => 'GOB',
        ]);
        BusinessActivity::create([
            'name' => 'Hoteleria',
            'abbreviation' => 'HOT',
        ]);
        BusinessActivity::create([
            'name' => 'Importación',
            'abbreviation' => 'IMP',
        ]);
        BusinessActivity::create([
            'name' => 'Industria',
            'abbreviation' => 'IND',
        ]);
        BusinessActivity::create([
            'name' => 'Joyeria',
            'abbreviation' => 'JOY',
        ]);
        BusinessActivity::create([
            'name' => 'Medicina',
            'abbreviation' => 'MED',
        ]);
        BusinessActivity::create([
            'name' => 'Muebleria',
            'abbreviation' => 'MUE',
        ]);
        BusinessActivity::create([
            'name' => 'Optica',
            'abbreviation' => 'OPT',
        ]);
        BusinessActivity::create([
            'name' => 'Plásticos desechables',
            'abbreviation' => 'PLA',
        ]);
        BusinessActivity::create([
            'name' => 'Publicidad',
            'abbreviation' => 'PUB',
        ]);
        BusinessActivity::create([
            'name' => 'Seguros',
            'abbreviation' => 'SEG',
        ]);
        BusinessActivity::create([
            'name' => 'Servicios',
            'abbreviation' => 'SER',
        ]);
        BusinessActivity::create([
            'name' => 'Telefonía',
            'abbreviation' => 'TEL',
        ]);
        BusinessActivity::create([
            'name' => 'Tequilera',
            'abbreviation' => 'TEQ',
        ]);
        BusinessActivity::create([
            'name' => 'Transporte',
            'abbreviation' => 'TRA',
        ]);
        BusinessActivity::create([
            'name' => 'Turismo',
            'abbreviation' => 'TUR',
        ]);
        BusinessActivity::create([
            'name' => 'Vestido / Textil',
            'abbreviation' => 'VES',
        ]);
        BusinessActivity::create([
            'name' => 'Veterinaria',
            'abbreviation' => 'VET',
        ]);
        BusinessActivity::create([
            'name' => 'Calzado',
            'abbreviation' => 'ZAP',
        ]);
    }
}
