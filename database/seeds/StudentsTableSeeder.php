<?php

use App\Models\Admin\Student\Student;
use Faker\Generator as Faker;
use Illuminate\Database\Seeder;

class StudentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(Faker $faker)
    {
        Student::create([
            'register' => 41241,
            'family_id' => 11677,
            'campus_id' => 1,
            'lastname' => $faker->lastname,
            'second_lastname' => $faker->lastname,
            'name' => $faker->name,
            'grade' => $faker->randomDigit,
            'group' => $faker->randomLetter,
            'section' => $faker->randomFloat(1, 0, 3),
            'letter' => $faker->numberBetween(0, 1),
            'entry_status' => $faker->randomLetter,
            'la_status' => $faker->numerify('F###'),
        ]);
        Student::create([
            'register' => 30051,
            'family_id' => 11677,
            'campus_id' => 1,
            'lastname' => $faker->lastname,
            'second_lastname' => $faker->lastname,
            'name' => $faker->name,
            'grade' => 6,
            'group' => $faker->randomLetter,
            'section' => 3,
            'letter' => $faker->numberBetween(0, 1),
            'entry_status' => 'P',
            'la_status' => $faker->numerify('F###'),
        ]);
        Student::create([
            'register' => 41555,
            'family_id' => 11677,
            'campus_id' => 1,
            'lastname' => $faker->lastname,
            'second_lastname' => $faker->lastname,
            'name' => $faker->name,
            'grade' => 5,
            'group' => $faker->randomLetter,
            'section' => 1,
            'letter' => $faker->numberBetween(0, 1),
            'entry_status' => 'N',
            'la_status' => $faker->numerify('F###'),
        ]);
        Student::create([
            'register' => $faker->randomNumber(),
            'family_id' => $faker->randomNumber(),
            'campus_id' => 2,
            'lastname' => $faker->lastname,
            'second_lastname' => $faker->lastname,
            'name' => $faker->name,
            'grade' => 3,
            'group' => $faker->randomLetter,
            'section' => .5,
            'letter' => $faker->numberBetween(0, 1),
            'entry_status' => 'S',
            'la_status' => 'CO_11677',
        ]);
        Student::create([
            'register' => $faker->randomNumber(),
            'family_id' => $faker->randomNumber(),
            'campus_id' => 2,
            'lastname' => $faker->lastname,
            'second_lastname' => $faker->lastname,
            'name' => $faker->name,
            'grade' => $faker->randomDigit,
            'group' => $faker->randomLetter,
            'section' => $faker->randomFloat(1, 0, 3),
            'letter' => $faker->numberBetween(0, 1),
            'entry_status' => $faker->randomLetter,
            'la_status' => $faker->numerify('LA_###'),
        ]);
        //factory(Student::class, 200)->create();
    }
}
