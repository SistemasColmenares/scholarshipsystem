<?php

use App\Models\GeneralResources\Campus\Campus;
use Illuminate\Database\Seeder;

class CampusesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Campus::create([
            'name' => 'Todos',
            'sie_id' => 0,
        ]);
        Campus::create([
            'name' => 'Liceo del Valle',
            'sie_id' => 1,
        ]);
        Campus::create([
            'name' => 'Altamira Moctezuma',
            'sie_id' => 4,
        ]);
        Campus::create([
            'name' => 'Torreblanca',
            'sie_id' => 3,
        ]);
        Campus::create([
            'name' => 'Altamira La Cima',
            'sie_id' => 21,
        ]);
        Campus::create([
            'name' => 'Los Altos',
            'sie_id' => 2,
        ]);
        Campus::create([
            'name' => 'Ermita',
            'sie_id' => 12,
        ]);
        Campus::create([
            'name' => 'Modena',
            'sie_id' => 11,
        ]);
        Campus::create([
            'name' => 'Montevideo',
            'sie_id' => 13,
        ]);
        Campus::create([
            'name' => 'Valle',
            'sie_id' => 14,
        ]);
        Campus::create([
            'name' => 'Universidad Panamericana',
            'sie_id' => 99,
        ]);
        Campus::create([
            'name' => 'Beelieve',
            'sie_id' => 15,
        ]);
        Campus::create([
            'name' => 'Jarales',
            'sie_id' => 20,
        ]);
        Campus::create([
            'name' => 'Granja',
            'sie_id' => 16,
        ]);
        Campus::create([
            'name' => 'La cima Tzitlacalli',
            'sie_id' => 17,
        ]);
    }
}
