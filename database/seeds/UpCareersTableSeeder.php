<?php

use App\Models\GeneralResources\UpCareer\UpCareer;
use Illuminate\Database\Seeder;

class UpCareersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        UpCareer::create([
            'name' => 'Administración y Dirección',
            'abbreviation' => 'AD',
        ]);
        UpCareer::create([
            'name' => 'Administración y Dirección de Empresas Familiares',
            'abbreviation' => 'AEF',
        ]);
        UpCareer::create([
            'name' => 'Administración y Finanzas',
            'abbreviation' => 'AF',
        ]);
        UpCareer::create([
            'name' => 'Administración y Mercadotecnia',
            'abbreviation' => 'AM',
        ]);
        UpCareer::create([
            'name' => 'Administración y Negocios Internacionales',
            'abbreviation' => 'ANI',
        ]);
        UpCareer::create([
            'name' => 'Administración y Recursos Humanos',
            'abbreviation' => 'ARH',
        ]);
        UpCareer::create([
            'name' => 'Comunicación y Creación Audiovisual',
            'abbreviation' => 'AUD',
        ]);
        UpCareer::create([
            'name' => 'Comunicación, Publicidad y Relaciones Públicas',
            'abbreviation' => 'PUB',
        ]);
        UpCareer::create([
            'name' => 'Contaduría',
            'abbreviation' => 'CP',
        ]);
        UpCareer::create([
            'name' => 'Derecho',
            'abbreviation' => 'DER',
        ]);
        UpCareer::create([
            'name' => 'Administración y Hospitalidad',
            'abbreviation' => 'ESD AL',
        ]);
        UpCareer::create([
            'name' => 'Ingeniería en Animación Digital',
            'abbreviation' => 'IAD',
        ]);
        UpCareer::create([
            'name' => 'Ingeniería Civil y Administración',
            'abbreviation' => 'ICA',
        ]);
        UpCareer::create([
            'name' => 'Ingeniería en Innovación y Diseño',
            'abbreviation' => 'IID',
        ]);
        UpCareer::create([
            'name' => 'Ingeniería Industrial e Innovación de Negocios',
            'abbreviation' => 'IIN',
        ]);
        UpCareer::create([
            'name' => 'Ingeniería Mecatrónica',
            'abbreviation' => 'IME',
        ]);
        UpCareer::create([
            'name' => 'Comunicación y Periodismo',
            'abbreviation' => 'PER',
        ]);
        UpCareer::create([
            'name' => 'Pedagogía Innovación Educativa',
            'abbreviation' => 'PIE',
        ]);
        UpCareer::create([
            'name' => 'Psicopedagogía',
            'abbreviation' => 'PSP',
        ]);
        UpCareer::create([
            'name' => 'Ingeniería en Electrónica y Sistemas Digitales',
            'abbreviation' => 'IESD',
        ]);
        UpCareer::create([
            'name' => 'Ingeniería en Sistemas y Gráficas Computacionales',
            'abbreviation' => 'ISGC',
        ]);
        UpCareer::create([
            'name' => 'Arquitectura',
            'abbreviation' => 'ARQ',
        ]);
    }
}
