<?php

use App\Models\GeneralResources\UpStatus\UpStatus;
use Illuminate\Database\Seeder;

class UpStatusesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        UpStatus::create([
            'name' => 'Candidato',
        ]);
        UpStatus::create([
            'name' => 'No candidato',
        ]);
        UpStatus::create([
            'name' => 'Sin validación',
        ]);
    }
}
