<?php

use App\Models\General\UpStudent\UpStudent;
use Illuminate\Database\Seeder;

class UpStudentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(UpStudent::class, 200)->create();
    }
}
