<?php

use App\Models\User\Permission\Permission;
use Illuminate\Database\Seeder;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Permission::create([
            'create' => 1,
            'read' => 1,
            'update' => 1,
            'delete' => 1,
            'role_id' => 1,
        ]);
        Permission::create([
            'create' => 1,
            'read' => 1,
            'update' => 1,
            'delete' => 0,
            'role_id' => 2,
        ]);
        Permission::create([
            'create' => 0,
            'read' => 1,
            'update' => 0,
            'delete' => 0,
            'role_id' => 3,
        ]);
    }
}
