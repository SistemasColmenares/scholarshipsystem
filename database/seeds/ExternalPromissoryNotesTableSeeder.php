<?php

use App\Models\Admin\ExternalPromissoryNote\ExternalPromissoryNote;
use Illuminate\Database\Seeder;

class ExternalPromissoryNotesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(ExternalPromissoryNote::class, 50)->create();
    }
}
