<?php

use App\Models\Admin\UserFamily\UserFamily;
use Faker\Generator as Faker;
use Illuminate\Database\Seeder;

class UserFamiliesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(Faker $faker)
    {
        UserFamily::create([
            'family_id' => 11677,
            'name' => 'Chávez Zárate',
            'code' => 'F11677',
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
            'terms_and_conditions' => 0,
            'full_access' => 1,
            'individual_dates' => 0,
        ]);
        //factory(UserFamily::class, 50)->create();
    }
}
