<?php

use App\Models\User\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        User::create([
            'campus_id' => 0,
            'role_id' => 1,
            'name' => 'Superadmin',
            'email' => 'superadmin@admin.com',
            'password' => Hash::make('superadmin'),
        ]);
        User::create([
            'campus_id' => 0,
            'role_id' => 2,
            'name' => 'Admin',
            'email' => 'admin@admin.com',
            'password' => Hash::make('admin'),
        ]);
        User::create([
            'campus_id' => 4,
            'role_id' => 2,
            'name' => 'Chavez Zárate',
            'email' => 'salvador.chavez@colmenares.org.mx',
            'password' => Hash::make('chavez_alta'),
        ]);
        //factory(User::class, 5)->create();

    }
}
