<?php

use App\Models\GeneralResources\Schoolarship\Schoolarship;
use Illuminate\Database\Seeder;

class SchoolarshipsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Schoolarship::create([
            'name' => 'Interna / Oficial',
        ]);
        Schoolarship::create([
            'name' => 'Apoyo a familia numerosa',
        ]);
        Schoolarship::create([
            'name' => 'Orfandad',
        ]);
    }
}
