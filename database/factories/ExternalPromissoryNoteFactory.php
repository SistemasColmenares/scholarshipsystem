<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Admin\ExternalPromissoryNote\ExternalPromissoryNote;
use Faker\Generator as Faker;

$factory->define(ExternalPromissoryNote::class, function (Faker $faker) {
    return [
        'family_id' => $faker->randomNumber(),
        'family' => $faker->name,
        'register' => $faker->randomNumber(),
        'student' => $faker->name,
    ];
});
