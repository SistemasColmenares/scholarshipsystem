<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\General\UpStudent\UpStudent;
use Faker\Generator as Faker;

$factory->define(UpStudent::class, function (Faker $faker) {
    return [
        'register' => $faker->randomNumber(),
        'family_id' => $faker->randomNumber(),
        'campus_id' => 99,
        'lastname' => $faker->lastname,
        'second_lastname' => $faker->lastname,
        'name' => $faker->name,
        'semester' => $faker->randomDigit,
        'career_id' => $faker->randomDigit,
        'up_status_id' => $faker->numberBetween(1, 3),
    ];
});
