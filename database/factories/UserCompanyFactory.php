<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Admin\UserCompany\UserCompany;
use Faker\Generator as Faker;

$factory->define(UserCompany::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
        'image' => 'https://placeholder.com/300x600',
        'image_name' => $faker->words(3, true),
    ];
});
