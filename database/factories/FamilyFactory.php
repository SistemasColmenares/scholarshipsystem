<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Admin\Family\Family;
use Faker\Generator as Faker;

$factory->define(Family::class, function (Faker $faker) {
    return [
        'family_id' => $faker->randomNumber(),
        'address' => $faker->address,
        'pc' => $faker->randomNumber(),
        'suburb' => $faker->streetName,
        'phone_number' => $faker->numerify('##########'),
        'ft_lastname' => $faker->lastname,
        'ft_second_lastname' => $faker->lastname,
        'ft_name' => $faker->name,
        'ft_birthdate' => $faker->date,
        'ft_email' => $faker->email,
        'ft_cellphone_number' => $faker->numerify('##########'),
        'ft_business_name' => $faker->company,
        'ft_business_activity' => $faker->randomLetter,
        'ft_business_position' => $faker->jobTitle,
        'ft_business_antiquity' => $faker->randomDigit(),
        'ft_business_society' => $faker->randomLetter,
        'ft_business_percent' => $faker->randomFloat(1, 0, 100),
        'mt_lastname' => $faker->lastname,
        'mt_second_lastname' => $faker->lastname,
        'mt_name' => $faker->name,
        'mt_birthdate' => $faker->date,
        'mt_email' => $faker->email,
        'mt_cellphone_number' => $faker->numerify('##########'),
        'mt_business_name' => $faker->company,
        'mt_business_activity' => $faker->randomLetter,
        'mt_business_position' => $faker->jobTitle,
        'mt_business_antiquity' => $faker->randomDigit(),
        'mt_business_society' => $faker->randomLetter,
        'mt_business_percent' => $faker->randomFloat(1, 0, 100),
    ];
});
