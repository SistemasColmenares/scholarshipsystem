<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Admin\UserFamily\UserFamily;
use Faker\Generator as Faker;

$factory->define(UserFamily::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'code' => $faker->unique()->numerify('F####'),
        'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
        'family_id' => $faker->randomNumber(),
    ];
});
