<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Admin\Settings\Settings;
use Faker\Generator as Faker;

$factory->define(Settings::class, function (Faker $faker) {
    return [
        'start_date' => '2021-01-01',
        'end_date' => '2021-12-31',
        'research_cost' => $faker->randomFloat(2, 0, 1000),
        'research_date' => $faker->date,
        'bank_reference' => $faker->randomNumber(),
        'clabe' => $faker->randomNumber(),
        'account' => $faker->randomNumber(),
        'inscription_percent' => $faker->randomFloat(2, 0, 100),
        'three_children_percent' => $faker->randomFloat(2, 0, 100),
        'four_up_children_percent' => $faker->randomFloat(2, 0, 100),
    ];
});
