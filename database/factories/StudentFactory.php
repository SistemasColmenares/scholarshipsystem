<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Admin\Student\Student;
use Faker\Generator as Faker;

$factory->define(Student::class, function (Faker $faker) {
    return [
        'register' => $faker->randomNumber(),
        'family_id' => $faker->randomNumber(),
        'campus_id' => $faker->randomNumber(),
        'lastname' => $faker->lastname,
        'second_lastname' => $faker->lastname,
        'name' => $faker->name,
        'grade' => $faker->randomDigit,
        'group' => $faker->randomLetter,
        'section' => $faker->randomFloat(1, 0, 3),
        'letter' => $faker->randomLetter,
        'entry_status' => $faker->randomLetter,
        'la_status' => $faker->numerify('F###'),
    ];
});
