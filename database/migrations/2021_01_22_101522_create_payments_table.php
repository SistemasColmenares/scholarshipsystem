<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('campus_id')->nullable();
            $table->integer('student')->nullable();
            $table->string('section')->nullable();
            $table->integer('concept')->nullable();
            $table->integer('month')->nullable();
            $table->integer('month_pay')->nullable();
            $table->decimal('import', 8, 2)->nullable();
            $table->decimal('recharge', 8, 2)->nullable();
            $table->dateTime('date_payment')->nullable();
            $table->string('observation', 15)->nullable();
            $table->integer('chek')->nullable();
            $table->string('bank', 3)->nullable();
            $table->integer('period')->nullable();
            $table->integer('article')->nullable();
            $table->decimal('vat', 5, 2)->nullable();
            $table->integer('bill')->nullable();
            $table->string('type', 1)->nullable();
            $table->string('account_import', 12)->nullable();
            $table->string('account_recharge', 12)->nullable();
            $table->string('account_vat', 12)->nullable();
            $table->decimal('normal_racharge', 8, 2)->nullable();
            $table->decimal('credit_note', 8, 2)->nullable();
            $table->integer('credit_note_folio')->nullable();
            $table->integer('consecutive')->nullable();
            $table->string('payment_usd', 1)->nullable();
            $table->string('payment_type', 3)->nullable();
            $table->decimal('change_usd', 9, 2)->nullable();
            $table->decimal('comission', 8, 2)->nullable();
            $table->integer('pay_period')->nullable();
            $table->integer('student_record')->nullable();
            $table->string('account_comission', 12)->nullable();
            $table->string('serie')->nullable();
            $table->decimal('fe_folio')->nullable();
            $table->decimal('discount', 8, 2)->nullable();
            $table->string('account_discount', 12)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payments');
    }
}
