<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUpStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('up_students', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('register');
            $table->integer('family_id');
            $table->integer('campus_id');
            $table->string('lastname');
            $table->string('second_lastname');
            $table->string('name');
            $table->smallInteger('semester');
            $table->integer('career_id');
            $table->float('schoolarship_percent')->nullable();
            $table->integer('up_status_id');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('up_students');
    }
}
