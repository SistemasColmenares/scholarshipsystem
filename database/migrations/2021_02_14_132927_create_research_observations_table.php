<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateResearchObservationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('research_observations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('family_id');
            $table->longText('family')->nullable();
            $table->longText('student')->nullable();
            $table->longText('economic_dependent')->nullable();
            $table->longText('monthly_income')->nullable();
            $table->longText('monthly_expenses')->nullable();
            $table->longText('vehicle')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('research_observations');
    }
}
