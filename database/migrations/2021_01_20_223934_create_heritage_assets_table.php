<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHeritageAssetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('heritage_assets', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('family_id');
            $table->boolean('house')->nullable();
            $table->boolean('mortgage')->nullable();
            $table->boolean('rent')->nullable();
            $table->boolean('condominium')->nullable();
            $table->boolean('ground')->nullable();
            $table->boolean('cottage')->nullable();
            $table->boolean('property')->nullable();
            $table->boolean('club')->nullable();
            $table->string('club_name')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('heritage_assets');
    }
}
