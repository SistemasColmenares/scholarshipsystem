<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAfnImportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('afn_imports', function (Blueprint $table) {
            $table->integer('student');
            $table->float('import');
            $table->float('import_month');
            $table->integer('cycle');
            $table->integer('concept');
            $table->integer('student_counter')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('afn_imports');
    }
}
