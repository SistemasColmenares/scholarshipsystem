<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFamiliesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('families', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('family_id')->unique();
            $table->integer('company_id')->nullable();
            $table->integer('bank_reference')->nullable();
            $table->string('address')->nullable();
            $table->string('town')->nullable();
            $table->integer('pc')->nullable();
            $table->string('suburb', 40)->nullable();
            $table->string('phone_number', 20)->nullable();
            $table->string('ft_lastname')->nullable();
            $table->string('ft_second_lastname')->nullable();
            $table->string('ft_name')->nullable();
            $table->date('ft_birthdate')->nullable();
            $table->tinyInteger('ft_age')->nullable();
            $table->string('ft_email')->nullable();
            $table->string('ft_cellphone_number', 20)->nullable();
            $table->string('ft_business_name')->nullable();
            $table->string('ft_business_activity')->nullable();
            $table->string('ft_business_position')->nullable();
            $table->string('ft_business_antiquity')->nullable();
            $table->string('ft_business_society')->nullable();
            $table->float('ft_business_percent')->nullable();
            $table->string('mt_lastname')->nullable();
            $table->string('mt_second_lastname')->nullable();
            $table->string('mt_name')->nullable();
            $table->date('mt_birthdate')->nullable();
            $table->tinyInteger('mt_age')->nullable();
            $table->string('mt_email')->nullable();
            $table->string('mt_cellphone_number', 20)->nullable();
            $table->string('mt_business_name')->nullable();
            $table->string('mt_business_activity')->nullable();
            $table->string('mt_business_position')->nullable();
            $table->string('mt_business_antiquity')->nullable();
            $table->string('mt_business_society')->nullable();
            $table->float('mt_business_percent')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('families');
    }
}
