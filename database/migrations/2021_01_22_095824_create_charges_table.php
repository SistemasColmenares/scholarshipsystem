<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateChargesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('charges', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('campus_id')->nullable();
            $table->integer('student')->nullable();
            $table->string('section')->nullable();
            $table->decimal('concept', 3, 0)->nullable();
            $table->integer('month')->nullable();
            $table->integer('month_pay')->nullable();
            $table->decimal('import', 8, 2)->nullable();
            $table->string('pay', 1)->nullable();
            $table->integer('period')->nullable();
            $table->string('type', 1)->nullable();
            $table->decimal('recharge_m', 8, 2)->nullable();
            $table->string('schoolarship')->nullable();
            $table->decimal('schoolarship_percent')->nullable();
            $table->string('send_by_inverlat', 1)->nullable();
            $table->string('temp_recharge')->nullable();
            $table->decimal('vat', 5, 2)->nullable();
            $table->integer('pay_period')->nullable();
            $table->integer('student_record')->nullable();
            $table->string('schoolarship_two')->nullable();
            $table->string('schoolarship_percent_two')->nullable();
            $table->decimal('concept_import', 8, 2)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('charges');
    }
}
