<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateResearchHeritageAssetsDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('research_heritage_assets_details', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('family_id');
            $table->string('property')->nullable();
            $table->string('location')->nullable();
            $table->float('property_price')->nullable();
            $table->string('owner')->nullable();
            $table->integer('permanence')->nullable();
            $table->integer('people')->nullable();
            $table->string('property_type')->nullable();
            $table->string('property_status')->nullable();
            $table->float('surface')->nullable();
            $table->float('building')->nullable();
            $table->float('building_price')->nullable();
            $table->string('added_value')->nullable();
            $table->integer('bedroom')->nullable();
            $table->integer('bathroom')->nullable();
            $table->integer('living_room')->nullable();
            $table->integer('dinning_room')->nullable();
            $table->integer('yard')->nullable();
            $table->integer('tv')->nullable();
            $table->integer('computer')->nullable();
            $table->integer('dvd_bluray_videogame')->nullable();
            $table->integer('smartdevice')->nullable();
            $table->integer('refrigerator')->nullable();
            $table->integer('cellphone_tablet')->nullable();
            $table->integer('gardener')->nullable();
            $table->integer('domestic_employee')->nullable();
            $table->integer('driver')->nullable();
            $table->integer('kitchener')->nullable();
            $table->integer('cable_phone_internet')->nullable();
            $table->integer('streaming_service')->nullable();
            $table->longText('general_conditions')->nullable();
            $table->longText('observation')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('research_heritage_assets_details');
    }
}
