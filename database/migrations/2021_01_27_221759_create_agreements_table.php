<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAgreementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agreements', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('campus_id')->nullable();
            $table->integer('agreement_folio')->nullable();
            $table->integer('student')->nullable();
            $table->string('name', 300)->nullable();
            $table->string('concept', 200)->nullable();
            $table->string('month', 4)->nullable();
            $table->integer('schoolar_cycle')->nullable();
            $table->date('date_payment')->nullable();
            $table->decimal('debit', 10, 2)->nullable();
            $table->decimal('surcharge', 10, 2)->nullable();
            $table->string('status', 1)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('agreements');
    }
}
