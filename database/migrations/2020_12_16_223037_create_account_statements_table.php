<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAccountStatementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('account_statements', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('campus');
            $table->integer('student_id');
            $table->string('name', 150)->nullable();
            $table->integer('family_id')->nullable();
            $table->string('concept_id', 10);
            $table->string('concept', 100);
            $table->string('month', 50);
            $table->date('payment_date')->nullable();
            $table->string('schoolar_cycle', 4);
            $table->decimal('debit', 18, 2);
            $table->decimal('surcharge', 18, 2);
            $table->integer('agreement_folio')->nullable();
            $table->string('agreement', 1)->nullable();
            $table->string('type', 2)->nullable();
            $table->string('paid', 1)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('account_statements');
    }
}
