<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('students', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('register')->nullable();
            $table->integer('family_id')->nullable();
            $table->integer('campus_id')->nullable();
            $table->string('lastname')->nullable();
            $table->string('second_lastname')->nullable();
            $table->string('name')->nullable();
            $table->smallInteger('grade')->nullable();
            $table->string('group', 4)->nullable();
            $table->string('section')->nullable();
            $table->boolean('letter')->nullable();
            $table->string('entry_status', 2)->nullable();
            $table->string('la_status')->nullable();
            $table->float('schoolarship_percent')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('students');
    }
}
