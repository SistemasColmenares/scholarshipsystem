<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateExternalPromissoryNotesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('external_promissory_notes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('family_id', 45);
            $table->string('family', 30)->nullable();
            $table->string('register', 45);
            $table->string('student', 30)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('external_promissory_notes');
    }
}
