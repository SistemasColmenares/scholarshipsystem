<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateResearchFamilyHealthsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('research_family_healths', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('family_id');
            $table->longText('health_status')->nullable();
            $table->longText('member_disease')->nullable();
            $table->longText('medical_service')->nullable();
            $table->boolean('assured')->nullable();
            $table->float('insurance_policy_cost', 10, 2)->nullable();
            $table->float('insured_amount', 10, 2)->nullable();
            $table->longText('observation')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('research_family_healths');
    }
}
