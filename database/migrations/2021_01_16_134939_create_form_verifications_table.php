<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFormVerificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('form_verifications', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('family_id');
            $table->integer('schoolarship_id');
            $table->boolean('family')->nullable();
            $table->boolean('student')->nullable();
            $table->boolean('heritage_assets')->nullable();
            $table->boolean('monthly_income')->nullable();
            $table->boolean('monthly_expenses')->nullable();
            $table->boolean('schoolarship_reason')->nullable();
            $table->boolean('finished')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('form_verifications');
    }
}
