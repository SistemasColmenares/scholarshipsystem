<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMonthlyExpensesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('monthly_expenses', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('family_id');
            $table->float('food')->nullable();
            $table->float('rent')->nullable();
            $table->float('lending')->nullable();
            $table->float('electric_power')->nullable();
            $table->float('water')->nullable();
            $table->float('gas')->nullable();
            $table->float('car')->nullable();
            $table->float('mortgage')->nullable();
            $table->float('predial')->nullable();
            $table->float('phone')->nullable();
            $table->float('cable')->nullable();
            $table->float('internet')->nullable();
            $table->float('tuition')->nullable();
            $table->float('extra_class')->nullable();
            $table->float('domestic_service')->nullable();
            $table->float('book')->nullable();
            $table->float('free_time')->nullable();
            $table->float('med')->nullable();
            $table->float('dentist')->nullable();
            $table->float('clothes')->nullable();
            $table->float('assured')->nullable();
            $table->float('trip')->nullable();
            $table->float('other')->nullable();
            $table->longText('other_description')->nullable();
            $table->float('total')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('monthly_expenses');
    }
}
