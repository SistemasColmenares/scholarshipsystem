<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateResearchFormVerificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('research_form_verifications', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('family_id');
            $table->integer('company_id');
            $table->boolean('family')->nullable();
            $table->boolean('student')->nullable();
            $table->boolean('economic_dependent')->nullable();
            $table->boolean('monthly_income')->nullable();
            $table->boolean('monthly_expenses')->nullable();
            $table->boolean('vehicle')->nullable();
            $table->boolean('heritage_assets_detail')->nullable();
            $table->boolean('family_health')->nullable();
            $table->boolean('leisure_time')->nullable();
            $table->boolean('economic_situation_and_conclusion')->nullable();
            $table->boolean('photos')->nullable();
            $table->boolean('finished')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('research_form_verifications');
    }
}
