<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateResearchLeisureTimesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('research_leisure_times', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('family_id');
            $table->string('free_time')->nullable();
            $table->string('free_time_frequency')->nullable();
            $table->string('free_time_place')->nullable();
            $table->string('vacation_frequency')->nullable();
            $table->string('vacation_place')->nullable();
            $table->string('last_vacation')->nullable();
            $table->longText('observation')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('research_leisure_times');
    }
}
