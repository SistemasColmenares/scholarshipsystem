<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Registro de alumno UP</title>
</head>
<body>
    <p>Buen día.</p>
    <p>Se ha registrado un nuevo alumno en el sistema de becas en la fecha y hora siguiente:  {{ $upStudent->created_at }}.</p>
    <p>Estos son los datos del alumno que se ha registrado:</p>
    <ul>
        <li>Nombre: {{ $upStudent->name }} {{$upStudent->lastname}} {{$upStudent->second_lastname}}</li>
        <li>Matrícula: {{ $upStudent->register }}</li>
        
    </ul>
    <p>Valida el estatus del alumno en el siguiente enlace:</p>
    <ul>
        <li>
            <a href="https://becas.colmenares.org.mx/upStudentValidation/{{ $upStudent->id}}">
               Validar estatus de alumno
            </a>
        </li>
    </ul>
</body>
</html>