<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Seguimiento de estatus alumno UP</title>
</head>
<body>
    <p>Buen día.</p>
    <p>Por medio del presente se informa que se actualizó el estatus del alumno: </p>
     <ul>
        <li><b>Nombre: </b>{{ $mailAttributes['name'] }} {{ $mailAttributes['lastname'] }} {{ $mailAttributes['second_lastname'] }}</li>
        <li><b>Matrícula: </b>{{ $mailAttributes['register'] }}</li>
        
    </ul>
    <p>El estatus es:  <h3>{{ $mailAttributes['status']}}</h3></p>
    <p>{{ $mailAttributes['message']}} </p>

</body>
</html>