<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Recuperación de contraseña</title>
</head>
<body>
    <p>Buen día.</p>
    <p>Se hizo una solicitud de recuperación de contraseña.</p>
    <p>Estos son los datos del usuario:</p>
    <ul>
        <li><b>Código:</b> {{ $mailAttributes['code'] }}</li>
        <li><b>Contraseña:</b> {{ $mailAttributes['password'] }}</li>
        
    </ul>
    <p>Puede validarlo en el siguiente enlace: </p>
    <h3><a href="https://becas.colmenares.org.mx/login">
        Becas {{date("Y").' - '. date("Y") + 1}}
    </h3>
    </p>
</body>
</html>