<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Solicitud de beca finalizada</title>
</head>
<body>
    <p>Buen día familia <b>{{$mailAttributes['family_name']}}</b>:</p>
    <p>Se concluyó el proceso de solicitud de beca <i>{{$mailAttributes['schoolarship']}}</i>.</p>
    <p>Puede visualizar y descargar su solicitud en el siguiente enlace: </p>
    <a href="https://becas.colmenares.org.mx/solicitud/{{ $mailAttributes['family_id']}}/{{$mailAttributes['schoolarship_id']}}">
        Solicitud de beca
     </a>
</body>
</html>