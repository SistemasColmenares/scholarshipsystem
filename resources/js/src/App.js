 import React, { Component } from 'react'
import ReactDOM from 'react-dom'
import { Router, Route, Switch } from 'react-router-dom';
import { createBrowserHistory } from "history";
//@material-ui
import { ThemeProvider } from '@material-ui/core';
//Core Components
import Example from './Example';
import ProtectedRoute from './components/ProtectedRoute/ProtectedRoute';

import theme from './theme';

const hist = createBrowserHistory();

class App extends Component {
    render () {
        return (
            <ThemeProvider theme={theme}>
                <Router history={hist}>
                    <Switch>
                        <Route exact path='/' component={Example} />
                    </Switch>
                </Router>
            </ThemeProvider>
        )
    }
}

ReactDOM.render(<App />, document.getElementById('app'))