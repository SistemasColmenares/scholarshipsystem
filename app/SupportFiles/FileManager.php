<?php

namespace App\SupportFiles;

use Illuminate\Support\Facades\Storage;
use Image;
use Symfony\Component\HttpFoundation\Response;

class FileManager
{
    public function __construct()
    {
        $name = '';
        $path = '';
    }

    /**
     * Upload the image
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public static function uploadImage($image, $width, $height)
    {
        try {
            $name = uniqid();
            $path = 'img/'.$name.'.jpg';
            $img = Image::make($image)
                ->fit($width, $height)
                ->encode('jpg', 50);
            Storage::disk('public')->put($path, $img);
            $path = Storage::url($path);
            $imageFile = new FileManager;
            $imageFile->name = $name;
            $imageFile->path = $path;

            return $imageFile;
        } catch (Exception $exception) {
            return response('Internal Server Error', Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}
