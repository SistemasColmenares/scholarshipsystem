<?php

namespace App\Mail;

use App\Models\General\UpStudent\UpStudent;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class UpStudentRegisterMail extends Mailable
{
    use Queueable, SerializesModels;

    public $upStudent;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(UpStudent $upStudent)
    {
        //dd($upStudent);
        $this->upStudent = $upStudent;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('up-register-mail')
            ->from(env('MAIL_SCHOOLARSHIP'), '[COLMENARES] - Becas '.date('Y').' - '.date('Y', strtotime('+1 year')))
            ->subject('Registro de alumno UP.');
    }
}
