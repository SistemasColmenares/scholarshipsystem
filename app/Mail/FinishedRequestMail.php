<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class FinishedRequestMail extends Mailable
{
    use Queueable, SerializesModels;

    public $mailAttributes;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($mailAttributes)
    {
        $this->mailAttributes = $mailAttributes;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('finished-request-mail')
            ->from(env('MAIL_SCHOOLARSHIP'), '[COLMENARES] - Becas '.date('Y').' - '.date('Y', strtotime('+1 year')))
            ->subject('¡Felicidades! - Solicitud de beca finalizada');
    }
}
