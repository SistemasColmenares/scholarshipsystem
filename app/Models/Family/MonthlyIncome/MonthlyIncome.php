<?php

namespace App\Models\Family\MonthlyIncome;

use Illuminate\Database\Eloquent\Model;

class MonthlyIncome extends Model
{
    protected $fillable = [
        'family_id',
        'father',
        'mother',
        'pantry_voucher',
        'other',
        'total',
    ];
}
