<?php

namespace App\Models\Family\EconomicDependent;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class EconomicDependent extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'family_id',
        'name',
        'age',
        'kinship',
        'ocupation',
    ];
}
