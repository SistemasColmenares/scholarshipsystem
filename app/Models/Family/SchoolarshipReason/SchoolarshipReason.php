<?php

namespace App\Models\Family\SchoolarshipReason;

use Illuminate\Database\Eloquent\Model;

class SchoolarshipReason extends Model
{
    protected $fillable = [
        'family_id',
        'description',
    ];
}
