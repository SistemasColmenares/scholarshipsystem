<?php

namespace App\Models\Family\Vehicle;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Vehicle extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'family_id',
        'brand',
        'model',
        'owner',
        'year',
    ];
}
