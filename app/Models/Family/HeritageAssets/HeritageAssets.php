<?php

namespace App\Models\Family\HeritageAssets;

use Illuminate\Database\Eloquent\Model;

class HeritageAssets extends Model
{
    protected $fillable = [
        'family_id',
        'house',
        'mortgage',
        'rent',
        'condominium',
        'ground',
        'cottage',
        'property',
        'club',
        'club_name',
    ];
}
