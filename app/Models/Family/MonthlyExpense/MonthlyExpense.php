<?php

namespace App\Models\Family\MonthlyExpense;

use Illuminate\Database\Eloquent\Model;

class MonthlyExpense extends Model
{
    protected $fillable = [
        'family_id',
        'food',
        'rent',
        'lending',
        'electric_power',
        'water',
        'gas',
        'car',
        'mortgage',
        'predial',
        'phone',
        'cable',
        'internet',
        'tuition',
        'extra_class',
        'domestic_service',
        'book',
        'free_time',
        'med',
        'dentist',
        'clothes',
        'assured',
        'trip',
        'other',
        'other_description',
        'total',
    ];
}
