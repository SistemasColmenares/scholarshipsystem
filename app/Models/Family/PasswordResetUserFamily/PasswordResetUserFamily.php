<?php

namespace App\Models\Family\PasswordResetUserFamily;

use Illuminate\Database\Eloquent\Model;

class PasswordResetUserFamily extends Model
{
    protected $fillable = [
        'family_id',
        'code',
        'password',
    ];
}
