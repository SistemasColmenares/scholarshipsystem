<?php

namespace App\Models\Family\FormVerification;

use App\Models\Admin\Family\Family;
use App\Models\Admin\Student\Student;
use App\Models\GeneralResources\Schoolarship\Schoolarship;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class FormVerification extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'family_id',
        'schoolarship_id',
        'family',
        'student',
        'heritage_assets',
        'monthly_income',
        'monthly_expenses',
        'schoolarship_reason',
        'finished',
    ];

    public function student()
    {
        return $this->hasMany(Student::class, 'family_id', 'family_id');
    }

    public function schoolarship()
    {
        return $this->hasOne(Schoolarship::class, 'id', 'schoolarship_id');
    }

    public function family()
    {
        return $this->hasOne(Family::class, 'family_id', 'family_id');
    }
}
