<?php

namespace App\Models\GeneralResources\Campus;

use Illuminate\Database\Eloquent\Model;

class Campus extends Model
{
    protected $fillable = [
        'name',
        'sie_id',
    ];
}
