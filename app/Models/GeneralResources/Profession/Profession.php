<?php

namespace App\Models\GeneralResources\Profession;

use Illuminate\Database\Eloquent\Model;

class Profession extends Model
{
    protected $fillable = [
        'name',
        'abbreviation',
    ];
}
