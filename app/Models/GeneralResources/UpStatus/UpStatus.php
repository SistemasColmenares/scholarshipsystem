<?php

namespace App\Models\GeneralResources\UpStatus;

use Illuminate\Database\Eloquent\Model;

class UpStatus extends Model
{
    protected $fillable = [
        'name',
    ];
}
