<?php

namespace App\Models\GeneralResources\UpCareer;

use Illuminate\Database\Eloquent\Model;

class UpCareer extends Model
{
    protected $fillable = [
        'name',
        'abbreviation',
    ];
}
