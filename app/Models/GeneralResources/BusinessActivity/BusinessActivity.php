<?php

namespace App\Models\GeneralResources\BusinessActivity;

use Illuminate\Database\Eloquent\Model;

class BusinessActivity extends Model
{
    protected $fillable = [
        'name',
        'abbreviation',
    ];
}
