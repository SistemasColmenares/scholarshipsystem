<?php

namespace App\Models\GeneralResources\Schoolarship;

use Illuminate\Database\Eloquent\Model;

class Schoolarship extends Model
{
    protected $fillable = [
        'name',
    ];
}
