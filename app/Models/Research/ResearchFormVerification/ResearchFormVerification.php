<?php

namespace App\Models\Research\ResearchFormVerification;

use App\Models\Admin\Family\Family;
use App\Models\Admin\Student\Student;
use App\Models\Admin\UserCompany\UserCompany;
use Illuminate\Database\Eloquent\Model;

class ResearchFormVerification extends Model
{
    protected $fillable = [
        'family_id',
        'company_id',
        'family',
        'student',
        'economic_dependent',
        'monthly_income',
        'monthly_expenses',
        'vehicle',
        'heritage_assets_detail',
        'family_health',
        'leisure_time',
        'economic_situation_and_conclusion',
        'photos',
        'finished',
    ];

    public function student()
    {
        return $this->hasMany(Student::class, 'family_id', 'family_id');
    }

    public function company()
    {
        return $this->hasOne(UserCompany::class, 'id', 'company_id');
    }

    public function familyObject()
    {
        return $this->hasOne(Family::class, 'family_id', 'family_id');
    }
}
