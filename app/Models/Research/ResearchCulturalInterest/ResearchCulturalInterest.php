<?php

namespace App\Models\Research\ResearchCulturalInterest;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ResearchCulturalInterest extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'family_id',
        'place',
        'name',
        'position',
    ];
}
