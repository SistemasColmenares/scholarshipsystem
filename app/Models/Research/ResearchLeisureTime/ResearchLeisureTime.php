<?php

namespace App\Models\Research\ResearchLeisureTime;

use Illuminate\Database\Eloquent\Model;

class ResearchLeisureTime extends Model
{
    protected $fillable = [
        'family_id',
        'free_time',
        'free_time_frequency',
        'free_time_place',
        'vacation_frequency',
        'vacation_place',
        'last_vacation',
        'observation',
    ];
}
