<?php

namespace App\Models\Research\ResearchReference;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ResearchReference extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'family_id',
        'name',
        'phone',
        'relationship',
    ];
}
