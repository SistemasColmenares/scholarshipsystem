<?php

namespace App\Models\Research\ResearchFamilyHealth;

use Illuminate\Database\Eloquent\Model;

class ResearchFamilyHealth extends Model
{
    protected $fillable = [
        'family_id',
        'health_status',
        'member_disease',
        'medical_service',
        'assured',
        'insurance_policy_cost',
        'insured_amount',
        'observation',
    ];
}
