<?php

namespace App\Models\Research\ResearchHeritageAssetsDetail;

use Illuminate\Database\Eloquent\Model;

class ResearchHeritageAssetsDetail extends Model
{
    protected $fillable = [
        'family_id',
        'property',
        'location',
        'property_price',
        'owner',
        'permanence',
        'people',
        'property_type',
        'property_status',
        'surface',
        'building',
        'building_price',
        'added_value',
        'bedroom',
        'bathroom',
        'living_room',
        'dinning_room',
        'yard',
        'tv',
        'computer',
        'dvd_bluray_videogame',
        'smartdevice',
        'refrigerator',
        'cellphone_tablet',
        'gardener',
        'domestic_employee',
        'driver',
        'kitchener',
        'cable_phone_internet',
        'streaming_service',
        'general_conditions',
        'observation',
    ];
}
