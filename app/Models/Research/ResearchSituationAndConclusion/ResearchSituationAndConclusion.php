<?php

namespace App\Models\Research\ResearchSituationAndConclusion;

use App\Models\Research\ResearchFormVerification\ResearchFormVerification;
use Illuminate\Database\Eloquent\Model;

class ResearchSituationAndConclusion extends Model
{
    protected $fillable = [
        'family_id',
        'situation',
        'conclusion',
    ];

    public function formVerification()
    {
        return $this->hasOne(ResearchFormVerification::class, 'family_id', 'family_id');
    }
}
