<?php

namespace App\Models\Research\ResearchImage;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ResearchImage extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'family_id',
        'image',
        'image_name',
    ];
}
