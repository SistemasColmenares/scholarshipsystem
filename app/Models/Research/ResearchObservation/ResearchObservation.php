<?php

namespace App\Models\Research\ResearchObservation;

use Illuminate\Database\Eloquent\Model;

class ResearchObservation extends Model
{
    protected $fillable = [
        'family_id',
        'family',
        'student',
        'economic_dependent',
        'monthly_income',
        'monthly_expenses',
        'vehicle',
    ];
}
