<?php

namespace App\Models\Admin\Settings;

use Illuminate\Database\Eloquent\Model;

class Settings extends Model
{
    protected $fillable = [
        'start_date',
        'end_date',
        'research_cost',
        'research_date',
        'bank_reference',
        'clabe',
        'account',
        'inscription_percent',
        'three_children_percent',
        'four_up_children_percent',
    ];
}
