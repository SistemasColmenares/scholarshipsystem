<?php

namespace App\Models\Admin\Charge;

use Illuminate\Database\Eloquent\Model;

class Charge extends Model
{
    protected $fillable = [
        'student',
        'section',
        'concept',
        'month',
        'month_pay',
        'import',
        'pay',
        'period',
        'type',
        'recharge_m',
        'schoolarship',
        'schoolarship_percent',
        'send_by_inverlat',
        'temp_recharge',
        'vat',
        'pay_period',
        'student_record',
        'schoolarship_two',
        'schoolarship_percent_two',
        'concept_import',
    ];
}
