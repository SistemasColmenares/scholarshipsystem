<?php

namespace App\Models\Admin\Payment;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    protected $fillable = [
        'student',
        'section',
        'concept',
        'month',
        'month_pay',
        'import',
        'recharge',
        'date_payment',
        'observation',
        'chek',
        'bank',
        'period',
        'article',
        'vat',
        'bill',
        'type',
        'account_import',
        'account_recharge',
        'account_vat',
        'normal_racharge',
        'credit_note',
        'credit_note_folio',
        'consecutive',
        'payment_usd',
        'payment_type',
        'change_usd',
        'comission',
        'pay_period',
        'student_record',
        'account_comission',
        'serie',
        'fe_folio',
        'discount',
        'account_discount',
    ];
}
