<?php

namespace App\Models\Admin\AccountStatement;

use Illuminate\Database\Eloquent\Model;

class AccountStatement extends Model
{
    protected $fillable = [
        'campus',
        'student_id',
        'name',
        'family_id',
        'concept_id',
        'concept',
        'month',
        'payment_date',
        'schoolar_cycle',
        'debit',
        'surcharge',
        'agreement_folio',
        'agreement',
        'type',
        'paid',
    ];
}
