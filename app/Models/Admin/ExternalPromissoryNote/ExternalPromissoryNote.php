<?php

namespace App\Models\Admin\ExternalPromissoryNote;

use Illuminate\Database\Eloquent\Model;

class ExternalPromissoryNote extends Model
{
    protected $fillable = [
        'family_id',
        'family',
        'register',
        'student',
    ];
}
