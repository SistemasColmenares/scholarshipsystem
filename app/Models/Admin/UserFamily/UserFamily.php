<?php

namespace App\Models\Admin\UserFamily;

use App\Models\Admin\Family\Family;
use App\Models\Admin\Student\Student;
use App\Models\Family\FormVerification\FormVerification;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Tymon\JWTAuth\Contracts\JWTSubject;

class UserFamily extends Authenticatable implements JWTSubject
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'family_id',
        'name',
        'code',
        'password',
        'terms_and_conditions',
        'full_access',
        'individual_dates',
        'start_date',
        'end_date',
        'observation',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];

    public function family()
    {
        return $this->hasOne(Family::class, 'family_id', 'family_id');
    }

    public function formVerification()
    {
        return $this->hasOne(FormVerification::class, 'family_id', 'family_id');
    }

    public function student()
    {
        return $this->hasMany(Student::class, 'family_id', 'family_id');
    }

    //JWT
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function getJWTCustomClaims()
    {
        return [];
    }
}
