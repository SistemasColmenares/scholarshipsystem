<?php

namespace App\Models\Admin\Student;

use App\Models\GeneralResources\Campus\Campus;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Student extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'register',
        'family_id',
        'campus_id',
        'lastname',
        'second_lastname',
        'name',
        'grade',
        'section',
        'letter',
        'entry_status',
        'la_status',
        'schoolarship_percent',
    ];

    public function campus()
    {
        return $this->hasOne(Campus::class, 'sie_id', 'campus_id');
    }
}
