<?php

namespace App\Models\Admin\Agreement;

use Illuminate\Database\Eloquent\Model;

class Agreement extends Model
{
    protected $fillable = [
        'campus_id',
        'agreement_folio',
        'student',
        'name',
        'concept',
        'month',
        'schoolar_cycle',
        'date_payment',
        'debit',
        'surcharge',
        'status',
    ];
}
