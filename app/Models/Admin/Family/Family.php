<?php

namespace App\Models\Admin\Family;

use App\Models\Admin\UserCompany\UserCompany;
use App\Models\Admin\UserFamily\UserFamily;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Family extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'family_id',
        'company_id',
        'bank_reference',
        'address',
        'town',
        'pc',
        'suburb',
        'phone_number',
        'ft_lastname',
        'ft_second_lastname',
        'ft_name',
        'ft_birthdate',
        'ft_email',
        'ft_cellphone_number',
        'ft_business_name',
        'ft_business_activity',
        'ft_business_position',
        'ft_business_antiquity',
        'ft_business_society',
        'ft_business_percent',
        'mt_lastname',
        'mt_second_lastname',
        'mt_name',
        'mt_birthdate',
        'mt_email',
        'mt_cellphone_number',
        'mt_business_name',
        'mt_business_activity',
        'mt_business_position',
        'mt_business_antiquity',
        'mt_business_society',
        'mt_business_percent',
    ];

    public function user()
    {
        return $this->hasOne(UserFamily::class, 'family_id', 'family_id');
    }

    public function company()
    {
        return $this->hasOne(UserCompany::class, 'id', 'company_id');
    }
}
