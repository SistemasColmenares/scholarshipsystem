<?php

namespace App\Models\General\UpStudent;

use App\Models\GeneralResources\Campus\Campus;
use App\Models\GeneralResources\UpCareer\UpCareer;
use App\Models\GeneralResources\UpStatus\UpStatus;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UpStudent extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'register',
        'family_id',
        'campus_id',
        'lastname',
        'second_lastname',
        'name',
        'semester',
        'career_id',
        'schoolarship_percent',
        'up_status_id',
    ];

    public function campus()
    {
        return $this->hasOne(Campus::class, 'sie_id', 'campus_id');
    }

    public function status()
    {
        return $this->hasOne(UpStatus::class, 'id', 'up_status_id');
    }

    public function career()
    {
        return $this->hasOne(UpCareer::class, 'id', 'career_id');
    }
}
