<?php

namespace App\Models\User\Role;

use App\Models\User\Permission\Permission;
use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $fillable = [
        'name',
    ];

    public function permission()
    {
        return $this->hasOne(Permission::class, 'role_id', 'id');
    }
}
