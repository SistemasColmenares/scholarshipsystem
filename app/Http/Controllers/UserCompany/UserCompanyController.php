<?php

namespace App\Http\Controllers\UserCompany;

use App\Http\Controllers\Controller;
use App\Models\Admin\UserCompany\UserCompany;
use App\SupportFiles\FileManager;
use Config;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use JWTAuth;

class UserCompanyController extends Controller
{
    public function __construct()
    {
        Config::set('jwt.user', UserCompany::class);
        Config::set('auth.providers.users.model', UserCompany::class);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $userCompanies = UserCompany::all();

        return $userCompanies;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:user_companies,email',
            'password' => 'required|string|min:6|confirmed',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|dimensions:min_width=100,min_height=100',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        if ($request->hasFile('image')) {
            $image = FileManager::uploadImage($request->image, 900, 500);
        }

        $user = UserCompany::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'image' => $image->path,
            'image_name' => $image->name,
        ]);

        $token = JWTAuth::fromUser($user);

        return response()->json(compact('user', 'token'), 201);

    }

    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show(UserCompany $userCompany)
    {
        return $userCompany;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\User\UserCompany  $user
     * @return \App\Models\User\UserCompany
     */
    public function showByUserId(Request $request)
    {
        $userByUserId = UserCompany::find($request->user_id);

        return $userByUserId;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Models\User\User  $user
     * @return \Illuminate\Http\Response
     */
    public function updatePassword(Request $request, $user)
    {

        $data = UserCompany::find($user);
        $validator = Validator::make($request->all(), [
            'password' => 'string|min:6|confirmed',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $data->password = Hash::make($request->password);

        $data->save();

        return response()->json($data);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Models\Admin\UserCompany\UserCompany  $userCompany
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $userCompany)
    {
        $data = UserCompany::find($userCompany);

        $validator = Validator::make($request->all(), [
            'name' => 'string|max:255',
            'email' => 'string|email|max:255',
            'password' => 'string|min:6|confirmed',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|dimensions:min_width=100,min_height=100',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        if ($request->hasFile('image')) {
            $image = FileManager::uploadImage($request->image, 900, 500);
        }
        Storage::disk('public')->delete($data->image);

        $data->name = $request->name;
        $data->email = $request->email;
        $data->password = Hash::make($request->password);
        $data->image = $image->path;
        $data->image_name = $image->name;

        $data->save();

        return response()->json($data);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(UserCompany $userCompany)
    {
        $imageToDelete = 'img/'.$userCompany->image_name.'.jpg';
        Storage::disk('public')->delete($imageToDelete);
        $userCompany->delete();

        return response()->json([
            'error' => 'user_destroyed',
            'message' => 'El usuario se ha eliminado',
        ]);
    }

    public function authenticate(Request $request)
    {
        // Grab credentials from the request

        $credentials = $request->only('email', 'password');

        try {
            // Attempt to verify the credentials and create a token for the user
            if (! $token = JWTAuth::attempt($credentials)) {
                return response()->json([
                    'error' => 'invalid_credentials',
                    'message' => 'Las credenciales ingresadas no son correctas.',
                ], 401);
            }
        } catch (JWTException $e) {
            // Something went wrong whilst attempting to encode the token
            return response()->json([
                'error' => 'could_not_create_token',
                'message' => 'Servicio no disponbile por el momento.',
            ], 422);
        }

        // all good so return the token
        $user = UserCompany::where('email', $request->get('email'))->get();

        return response()->json([
            'user' => $user,
            'token' => $token,
        ], 200);
    }
}
