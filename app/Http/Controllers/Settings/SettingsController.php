<?php

namespace App\Http\Controllers\Settings;

use App\Http\Controllers\Controller;
use App\Models\Admin\Settings\Settings;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class SettingsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $settings = Settings::first();

        return $settings;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Models\Admin\Settings\Settings  $settings
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $settings)
    {
        $data = Settings::find($settings);

        $validator = Validator::make($request->all(), [
            'start_date' => 'date|required',
            'end_date' => 'date|required',
            'research_cost' => 'required',
            'research_date' => 'date|required',
            'inscription_percent' => 'required',
            'three_children_percent' => 'required',
            'four_up_children_percent' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $data->start_date = $request->start_date;
        $data->end_date = $request->end_date;
        $data->research_cost = $request->research_cost;
        $data->research_date = $request->research_date;
        $data->inscription_percent = $request->inscription_percent;
        $data->three_children_percent = $request->three_children_percent;
        $data->four_up_children_percent = $request->four_up_children_percent;

        $data->save();

        return response()->json($data);
    }
}
