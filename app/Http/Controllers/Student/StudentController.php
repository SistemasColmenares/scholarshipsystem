<?php

namespace App\Http\Controllers\Student;

use App\Http\Controllers\Controller;
use App\Models\Admin\Student\Student;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class StudentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $students = Student::all();

        return $students;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //QUEDA PENDIENTE
    }

    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Student $student)
    {
        return $student;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Admin\Student\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function showStudentOfFamily($family)
    {
        $studentsOfFamily = Student::with('campus')->where('family_id', $family)->get();

        return $studentsOfFamily;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Models\Admin\Student\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $student)
    {
        $data = Student::find($student);

        $validator = Validator::make($request->all(), [
            'register' => 'integer|required',
            'family_id' => 'integer|required',
            'lastname' => 'string|max:255',
            'second_lastname' => 'string|max:255',
            'name' => 'string|max:255',
            'grade' => 'integer|required',
            'group' => 'string|max:1',
            'section' => 'required',
            'letter' => 'string|max:1',
            'entry_status' => 'string|max:255',
            'la_status' => 'string|max:255',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $data->register = $request->register;
        $data->family_id = $request->family_id;
        $data->lastname = $request->lastname;
        $data->second_lastname = $request->second_lastname;
        $data->name = $request->name;
        $data->grade = $request->grade;
        $data->group = $request->group;
        $data->section = $request->section;
        $data->letter = $request->letter;
        $data->entry_status = $request->entry_status;
        $data->la_status = $request->la_status;

        $data->save();

        return response()->json($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Models\Admin\Student\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function updateSchoolarshipPercent(Request $request, $student)
    {
        $data = Student::find($student);

        $validator = Validator::make($request->all(), [
            'schoolarship_percent' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $data->schoolarship_percent = $request->schoolarship_percent;

        $data->save();

        return response()->json($data);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(Student $student)
    {
        $student->delete();

        return response()->json([
            'success' => 'data_destroyed',
            'message' => 'Registro eliminado',
        ]);
    }
}
