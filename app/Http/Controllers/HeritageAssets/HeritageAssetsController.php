<?php

namespace App\Http\Controllers\HeritageAssets;

use App\Http\Controllers\Controller;
use App\Models\Family\HeritageAssets\HeritageAssets;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class HeritageAssetsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $heritageAssets = HeritageAssets::all();

        return $heritageAssets;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'family_id' => 'integer|required',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $heritageAsset = HeritageAssets::create([
            'family_id' => $request->family_id,
            'house' => $request->house,
            'mortgage' => $request->mortgage,
            'rent' => $request->rent,
            'condominium' => $request->condominium,
            'ground' => $request->ground,
            'cottage' => $request->cottage,
            'property' => $request->property,
            'club' => $request->club,
            'club_name' => $request->club_name,
        ]);

        return response()->json($heritageAsset);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Family\HeritageAssets\HeritageAssets  $heritageAssets
     * @return \Illuminate\Http\Response
     */
    public function show($family)
    {
        $heritageAssetsOfFamily = HeritageAssets::where('family_id', $family)->get();

        return $heritageAssetsOfFamily;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Models\Family\HeritageAssets\HeritageAssets  $heritageAssets
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $family)
    {
        $data = HeritageAssets::where('family_id', $family)->first();

        $data->house = $request->house;
        $data->mortgage = $request->mortgage;
        $data->rent = $request->rent;
        $data->condominium = $request->condominium;
        $data->ground = $request->ground;
        $data->cottage = $request->cottage;
        $data->property = $request->property;
        $data->club = $request->club;
        $data->club_name = $request->club_name;

        $data->save();

        return response()->json($data);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(HeritageAssets $heritageAssets)
    {
        $heritageAssets->delete();

        return response()->json([
            'success' => 'data_destroyed',
            'message' => 'Registro eliminado',
        ]);
    }
}
