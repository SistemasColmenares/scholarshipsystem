<?php

namespace App\Http\Controllers\Vehicle;

use App\Http\Controllers\Controller;
use App\Models\Family\Vehicle\Vehicle;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class VehicleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $vehicles = Vehicle::all();

        return $vehicles;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'family_id' => 'integer|required',
            'brand' => 'required|string|max:255',
            'model' => 'required|string|max:255',
            'owner' => 'required|string|max:255',
            'year' => 'required|integer',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $vehicle = Vehicle::create([
            'family_id' => $request->family_id,
            'brand' => $request->brand,
            'model' => $request->model,
            'owner' => $request->owner,
            'year' => $request->year,
        ]);

        return response()->json($vehicle);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Family\Vehicle\Vehicle  $vehicle
     * @return \Illuminate\Http\Response
     */
    public function show($family)
    {
        $vehicleOfFamily = Vehicle::where('family_id', $family)->get();

        return $vehicleOfFamily;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(Vehicle $vehicle)
    {
        $vehicle->delete();

        return response()->json([
            'success' => 'data_destroyed',
            'message' => 'Registro eliminado',
        ]);
    }
}
