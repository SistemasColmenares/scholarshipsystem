<?php

namespace App\Http\Controllers\MonthlyExpense;

use App\Http\Controllers\Controller;
use App\Models\Family\MonthlyExpense\MonthlyExpense;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class MonthlyExpenseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $monthlyExpenses = MonthlyExpense::all();

        return $monthlyExpenses;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'family_id' => 'integer|required',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $monthlyExpense = MonthlyExpense::create([
            'family_id' => $request->family_id,
            'food' => $request->food,
            'rent' => $request->rent,
            'lending' => $request->lending,
            'electric_power' => $request->electric_power,
            'water' => $request->water,
            'gas' => $request->gas,
            'car' => $request->car,
            'mortgage' => $request->mortgage,
            'predial' => $request->predial,
            'phone' => $request->phone,
            'cable' => $request->cable,
            'internet' => $request->internet,
            'tuition' => $request->tuition,
            'extra_class' => $request->extra_class,
            'domestic_service' => $request->domestic_service,
            'book' => $request->book,
            'free_time' => $request->free_time,
            'med' => $request->med,
            'dentist' => $request->dentist,
            'clothes' => $request->clothes,
            'assured' => $request->assured,
            'trip' => $request->trip,
            'other' => $request->other,
            'other_description' => $request->other_description,
            'total' => $request->total,
        ]);

        return response()->json($monthlyExpense);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Family\MonthlyExpense\MonthlyExpense  $monthlyExpense
     * @return \Illuminate\Http\Response
     */
    public function show($family)
    {
        $monthlyExpenseOfFamily = MonthlyExpense::where('family_id', $family)->get();

        return $monthlyExpenseOfFamily;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Models\Family\MonthlyExpense\MonthlyExpense  $monthlyExpense
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $family)
    {
        $data = MonthlyExpense::where('family_id', $family)->first();

        $data->family_id = $request->family_id;
        $data->food = $request->food;
        $data->rent = $request->rent;
        $data->lending = $request->lending;
        $data->electric_power = $request->electric_power;
        $data->water = $request->water;
        $data->gas = $request->gas;
        $data->car = $request->car;
        $data->mortgage = $request->mortgage;
        $data->predial = $request->predial;
        $data->phone = $request->phone;
        $data->cable = $request->cable;
        $data->internet = $request->internet;
        $data->tuition = $request->tuition;
        $data->extra_class = $request->extra_class;
        $data->domestic_service = $request->domestic_service;
        $data->book = $request->book;
        $data->free_time = $request->free_time;
        $data->med = $request->med;
        $data->dentist = $request->dentist;
        $data->clothes = $request->clothes;
        $data->assured = $request->assured;
        $data->trip = $request->trip;
        $data->other = $request->other;
        $data->other_description = $request->other_description;
        $data->total = $request->total;

        $data->save();

        return response()->json($data);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(MonthlyExpense $monthlyExpense)
    {
        $monthlyExpense->delete();

        return response()->json([
            'success' => 'data_destroyed',
            'message' => 'Registro eliminado',
        ]);
    }
}
