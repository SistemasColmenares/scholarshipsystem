<?php

namespace App\Http\Controllers\ExternalPromissoryNote;

use App\Http\Controllers\Controller;
use App\Models\Admin\ExternalPromissoryNote\ExternalPromissoryNote;

class ExternalPromissoryNoteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $externalPromissoryNotes = ExternalPromissoryNote::all();

        return $externalPromissoryNotes;
    }
}
