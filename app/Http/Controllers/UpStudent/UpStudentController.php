<?php

namespace App\Http\Controllers\UpStudent;

use App\Http\Controllers\Controller;
use App\Mail\UpStudentRegisterMail;
use App\Mail\UpStudentStatusMail;
use App\Models\Admin\Family\Family;
use App\Models\General\UpStudent\UpStudent;
use App\Models\GeneralResources\UpStatus\UpStatus;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Mail;

class UpStudentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $UpStudents = UpStudent::with('career', 'status')->get();

        return $UpStudents;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'register' => 'required',
            'family_id' => 'integer|required',
            'campus_id' => 'integer|required',
            'lastname' => 'string|max:255',
            'second_lastname' => 'string|max:255',
            'name' => 'string|max:255',
            'semester' => 'integer|required',
            'career_id' => 'required',
            'up_status_id' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $upStudent = UpStudent::create([
            'register' => $request->register,
            'family_id' => $request->family_id,
            'campus_id' => $request->campus_id,
            'lastname' => $request->lastname,
            'second_lastname' => $request->second_lastname,
            'name' => $request->name,
            'semester' => $request->semester,
            'career_id' => $request->career_id,
            'up_status_id' => $request->up_status_id,
        ]);

        $sendmail = Mail::to(env('MAIL_UP_REGISTER'))->bcc([env('MAIL_SCHOOLARSHIP'), env('MAIL_SYSTEMS_AREA')])
            ->send(new UpStudentRegisterMail($upStudent));

        if (empty($sendmail)) {
            return response()->json([
                'data' => $upStudent,
                'message' => 'Correo enviado.',
            ], 200);
        } else {
            return response()->json(['message' => 'Falla en envío de correo.'], 400);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\General\UpStudent\UpStudent  $upStudent
     * @return \Illuminate\Http\Response
     */
    public function showByFamily($family)
    {
        $upStudentsByFamily = UpStudent::with('campus', 'status')->where('family_id', $family)->get();

        return $upStudentsByFamily;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\General\UpStudent\UpStudent  $upStudent
     * @return \Illuminate\Http\Response
     */
    public function show($upStudent)
    {
        $data = UpStudent::where('id', $upStudent)->with('career', 'status')->first();

        return $data;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Models\General\UpStudent\UpStudent  $upStudent
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $upStudent)
    {

        $data = UpStudent::find($upStudent);

        $validator = Validator::make($request->all(), [
            'register' => 'required',
            'family_id' => 'integer|required',
            'campus_id' => 'integer|required',
            'lastname' => 'string|max:255',
            'second_lastname' => 'string|max:255',
            'name' => 'string|max:255',
            'semester' => 'integer|required',
            'career_id' => 'integer|required',
            'up_status_id' => 'integer|required',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $data->register = $request->register;
        $data->family_id = $request->family_id;
        $data->campus_id = $request->campus_id;
        $data->lastname = $request->lastname;
        $data->second_lastname = $request->second_lastname;
        $data->name = $request->name;
        $data->semester = $request->semester;
        $data->career_id = $request->career_id;
        $data->up_status_id = $request->up_status_id;

        $data->save();

        return response()->json($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Models\Admin\Student\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function updateSchoolarshipPercent(Request $request, $upStudent)
    {
        $data = UpStudent::find($upStudent);

        $validator = Validator::make($request->all(), [
            'schoolarship_percent' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $data->schoolarship_percent = $request->schoolarship_percent;

        $data->save();

        return response()->json($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Models\General\UpStudent\UpStudent  $upStudent
     * @return \Illuminate\Http\Response
     */
    public function updateUpStatus(Request $request, $upStudent)
    {

        $data = UpStudent::find($upStudent);

        $validator = Validator::make($request->all(), [
            'up_status_id' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $data->up_status_id = $request->up_status_id;

        $data->save();
        //Get family by family_id
        $family = Family::where('family_id', $data->family_id)->first();

        if ($request->up_status_id == 1) {
            //Status: Candidato
            $status = UpStatus::find(1);

            $mailAttributes = [
                'register' => $data->register,
                'name' => $data->name,
                'lastname' => $data->lastname,
                'second_lastname' => $data->second_lastname,
                'status' => $status->name,
                'message' => 'Puede continuar el proceso de solicitud: http://becas.colmenares.org.mx/login',
            ];
            if ($family->ft_email && ! $family->mt_email) {
                $sendmail = Mail::to(env('MAIL_UP_REGISTER'))
                    ->cc($family->ft_email)
                    ->bcc(env('MAIL_SCHOOLARSHIP'))
                    ->send(new UpStudentStatusMail($mailAttributes));
                if (empty($sendmail)) {
                    return response()->json([
                        'data' => $data,
                        'message' => 'Correo enviado.',
                    ], 200);
                } else {
                    return response()->json(['message' => 'Falla en envío de correo.'], 400);
                }

            } elseif (! $family->ft_email && $family->mt_email) {
                $sendmail = Mail::to(env('MAIL_UP_REGISTER'))
                    ->cc($family->mt_email)
                    ->bcc([env('MAIL_SCHOOLARSHIP'), env('MAIL_SYSTEMS_AREA')])
                    ->send(new UpStudentStatusMail($mailAttributes));
                if (empty($sendmail)) {
                    return response()->json([
                        'data' => $data,
                        'message' => 'Correo enviado.',
                    ], 200);
                } else {
                    return response()->json(['message' => 'Falla en envío de correo.'], 400);
                }

            } elseif ($family->ft_email && $family->mt_email) {
                $sendmail = Mail::to(env('MAIL_UP_REGISTER'))
                    ->cc([$family->ft_email, $family->mt_email])
                    ->bcc([env('MAIL_SCHOOLARSHIP'), env('MAIL_SYSTEMS_AREA')])
                    ->send(new UpStudentStatusMail($mailAttributes));
                if (empty($sendmail)) {
                    return response()->json([
                        'data' => $data,
                        'message' => 'Correo enviado.',
                    ], 200);
                } else {
                    return response()->json(['message' => 'Falla en envío de correo.'], 400);
                }
            }
        }
        if ($request->up_status_id == 2) {
            //Status: No candidato
            $status = UpStatus::find(2);
            $mailAttributes = [
                'register' => $data->register,
                'name' => $data->name,
                'lastname' => $data->lastname,
                'second_lastname' => $data->second_lastname,
                'status' => $status->name,
                'message' => 'Favor de comunicarse con el administrador para validar la información.',
            ];
            if ($family->ft_email && ! $family->mt_email) {
                $sendmail = Mail::to(env('MAIL_UP_REGISTER'))
                    ->cc($family->ft_email)
                    ->bcc([env('MAIL_SCHOOLARSHIP'), env('MAIL_SYSTEMS_AREA')])
                    ->send(new UpStudentStatusMail($mailAttributes));
                if (empty($sendmail)) {
                    return response()->json([
                        'data' => $data,
                        'message' => 'Correo enviado.',
                    ], 200);
                } else {
                    return response()->json(['message' => 'Falla en envío de correo.'], 400);
                }

            } elseif (! $family->ft_email && $family->mt_email) {
                $sendmail = Mail::to(env('MAIL_UP_REGISTER'))
                    ->cc($family->mt_email)
                    ->bcc([env('MAIL_SCHOOLARSHIP'), env('MAIL_SYSTEMS_AREA')])
                    ->send(new UpStudentStatusMail($mailAttributes));
                if (empty($sendmail)) {
                    return response()->json([
                        'data' => $data,
                        'message' => 'Correo enviado.',
                    ], 200);
                } else {
                    return response()->json(['message' => 'Falla en envío de correo.'], 400);
                }

            } elseif ($family->ft_email && $family->mt_email) {
                $sendmail = Mail::to(env('MAIL_UP_REGISTER'))
                    ->cc([$family->ft_email, $family->mt_email])
                    ->bcc([env('MAIL_SCHOOLARSHIP'), env('MAIL_SYSTEMS_AREA')])
                    ->send(new UpStudentStatusMail($mailAttributes));
                if (empty($sendmail)) {
                    return response()->json([
                        'data' => $data,
                        'message' => 'Correo enviado.',
                    ], 200);
                } else {
                    return response()->json(['message' => 'Falla en envío de correo.'], 400);
                }
                if ($family->ft_email && ! $family->mt_email) {
                    $sendmail = Mail::to(env('MAIL_UP_REGISTER'))
                        ->cc($family->ft_email)
                        ->bcc([env('MAIL_SCHOOLARSHIP'), env('MAIL_SYSTEMS_AREA')])
                        ->send(new UpStudentStatusMail($mailAttributes));
                    if (empty($sendmail)) {
                        return response()->json([
                            'data' => $data,
                            'message' => 'Correo enviado.',
                        ], 200);
                    } else {
                        return response()->json(['message' => 'Falla en envío de correo.'], 400);
                    }

                } elseif (! $family->ft_email && $family->mt_email) {
                    $sendmail = Mail::to(env('MAIL_UP_REGISTER'))
                        ->cc($family->mt_email)
                        ->bcc([env('MAIL_SCHOOLARSHIP'), env('MAIL_SYSTEMS_AREA')])
                        ->send(new UpStudentStatusMail($mailAttributes));
                    if (empty($sendmail)) {
                        return response()->json([
                            'data' => $data,
                            'message' => 'Correo enviado.',
                        ], 200);
                    } else {
                        return response()->json(['message' => 'Falla en envío de correo.'], 400);
                    }

                } elseif ($family->ft_email && $family->mt_email) {
                    $sendmail = Mail::to(env('MAIL_UP_REGISTER'))
                        ->cc($family->ft_email, $family->mt_email)
                        ->bcc([env('MAIL_SCHOOLARSHIP'), env('MAIL_SYSTEMS_AREA')])
                        ->send(new UpStudentStatusMail($mailAttributes));
                    if (empty($sendmail)) {
                        return response()->json([
                            'data' => $data,
                            'message' => 'Correo enviado.',
                        ], 200);
                    } else {
                        return response()->json(['message' => 'Falla en envío de correo.'], 400);
                    }
                }
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(UpStudent $upStudent)
    {
        $upStudent->delete();

        return response()->json([
            'success' => 'data_destroyed',
            'message' => 'Registro eliminado',
        ]);
    }
}
