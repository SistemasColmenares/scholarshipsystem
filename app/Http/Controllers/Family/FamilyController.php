<?php

namespace App\Http\Controllers\Family;

use App\Http\Controllers\Controller;
use App\Models\Admin\Family\Family;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class FamilyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $families = Family::with('user')->get();

        return $families;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Admin\Family\Family  $family
     * @return \Illuminate\Http\Response
     */
    public function show($family)
    {
        $data = Family::with('user')->find($family);

        return $data;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Admin\Family\Family  $family
     * @return \Illuminate\Http\Response
     */
    public function showFamiyByFamilyId($family)
    {
        $data = Family::where('family_id', $family)->first();

        return $data;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Models\Admin\Family\Family  $family
     * @return \Illuminate\Http\Response
     */
    public function updateCompanyId(Request $request, $family)
    {
        $data = Family::find($family);
        $validator = Validator::make($request->all(), [
            'company_id' => 'integer|required',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }
        $data->company_id = $request->company_id;
        $data->save();

        return response()->json($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Models\Admin\Family\Family  $family
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $family)
    {
        $data = Family::find($family);

        $validator = Validator::make($request->all(), [
            'bank_reference' => 'integer|required',
            'address' => 'string|max:255',
            'town' => 'string|max:255',
            'pc' => 'integer|required',
            'suburb' => 'string|max:255',
            'phone_number' => 'digits_between:8,10',
            'ft_lastname' => 'string|max:255',
            'ft_second_lastname' => 'string|max:255',
            'ft_name' => 'string|max:255',
            'ft_birthdate' => 'date|required',
            'ft_email' => 'email',
            'ft_cellphone_number' => 'digits:10',
            'mt_lastname' => 'string|max:255',
            'mt_second_lastname' => 'string|max:255',
            'mt_name' => 'string|max:255',
            'mt_birthdate' => 'date|required',
            'mt_email' => 'email',
            'mt_cellphone_number' => 'digits:10',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $fatherAge = Carbon::parse($request->ft_birthdate)->age;
        $motherAge = Carbon::parse($request->mt_birthdate)->age;

        $data->bank_reference = $request->bank_reference;
        $data->address = $request->address;
        $data->town = $request->town;
        $data->pc = $request->pc;
        $data->suburb = $request->suburb;
        $data->phone_number = $request->phone_number;
        $data->ft_lastname = $request->ft_lastname;
        $data->ft_second_lastname = $request->ft_second_lastname;
        $data->ft_name = $request->ft_name;
        $data->ft_birthdate = $request->ft_birthdate;
        $data->ft_age = $fatherAge;
        $data->ft_email = $request->ft_email;
        $data->ft_cellphone_number = $request->ft_cellphone_number;
        $data->ft_business_name = $request->ft_business_name;
        $data->ft_business_activity = $request->ft_business_activity;
        $data->ft_business_position = $request->ft_business_position;
        $data->ft_business_antiquity = $request->ft_business_antiquity;
        $data->ft_business_society = $request->ft_business_society;
        $data->ft_business_percent = $request->ft_business_percent;
        $data->mt_lastname = $request->mt_lastname;
        $data->mt_second_lastname = $request->mt_second_lastname;
        $data->mt_name = $request->mt_name;
        $data->mt_birthdate = $request->mt_birthdate;
        $data->mt_age = $motherAge;
        $data->mt_email = $request->mt_email;
        $data->mt_cellphone_number = $request->mt_cellphone_number;
        $data->mt_business_name = $request->mt_business_name;
        $data->mt_business_activity = $request->mt_business_activity;
        $data->mt_business_position = $request->mt_business_position;
        $data->mt_business_antiquity = $request->mt_business_antiquity;
        $data->mt_business_society = $request->mt_business_society;
        $data->mt_business_percent = $request->mt_business_percent;

        $data->save();

        return response()->json($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Models\Admin\Family\Family  $family
     * @return \Illuminate\Http\Response
     */
    public function updateFamilyByFamilyId(Request $request, $family)
    {
        $data = Family::where('family_id', $family)->first();

        $validator = Validator::make($request->all(), [
            'address' => 'string|max:255',
            'town' => 'string|max:255',
            'pc' => 'integer|required',
            'suburb' => 'string|max:255',
            'phone_number' => 'digits_between:8,10',
            'ft_lastname' => 'string|max:255',
            'ft_second_lastname' => 'string|max:255',
            'ft_name' => 'string|max:255',
            'ft_birthdate' => 'date|required',
            'ft_email' => 'email',
            'ft_cellphone_number' => 'digits:10',
            'ft_business_name' => 'string|max:255',
            'ft_business_position' => 'string|max:255',
            'mt_lastname' => 'string|max:255',
            'mt_second_lastname' => 'string|max:255',
            'mt_name' => 'string|max:255',
            'mt_birthdate' => 'date|required',
            'mt_email' => 'email',
            'mt_cellphone_number' => 'digits:10',
            'mt_business_name' => 'string|max:255',
            'mt_business_position' => 'string|max:255',

        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $fatherAge = Carbon::parse($request->ft_birthdate)->age;
        $motherAge = Carbon::parse($request->mt_birthdate)->age;

        $data->address = $request->address;
        $data->town = $request->town;
        $data->pc = $request->pc;
        $data->suburb = $request->suburb;
        $data->phone_number = $request->phone_number;
        $data->ft_lastname = $request->ft_lastname;
        $data->ft_second_lastname = $request->ft_second_lastname;
        $data->ft_name = $request->ft_name;
        $data->ft_birthdate = $request->ft_birthdate;
        $data->ft_age = $fatherAge;
        $data->ft_email = $request->ft_email;
        $data->ft_cellphone_number = $request->ft_cellphone_number;
        $data->ft_business_name = $request->ft_business_name;
        $data->ft_business_activity = $request->ft_business_activity;
        $data->ft_business_position = $request->ft_business_position;
        $data->ft_business_antiquity = $request->ft_business_antiquity;
        $data->ft_business_society = $request->ft_business_society;
        $data->ft_business_percent = $request->ft_business_percent;
        $data->mt_lastname = $request->mt_lastname;
        $data->mt_second_lastname = $request->mt_second_lastname;
        $data->mt_name = $request->mt_name;
        $data->mt_birthdate = $request->mt_birthdate;
        $data->mt_age = $motherAge;
        $data->mt_email = $request->mt_email;
        $data->mt_cellphone_number = $request->mt_cellphone_number;
        $data->mt_business_name = $request->mt_business_name;
        $data->mt_business_activity = $request->mt_business_activity;
        $data->mt_business_position = $request->mt_business_position;
        $data->mt_business_antiquity = $request->mt_business_antiquity;
        $data->mt_business_society = $request->mt_business_society;
        $data->mt_business_percent = $request->mt_business_percent;

        $data->save();

        return response()->json($data);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(Family $family)
    {
        $family->delete();

        return response()->json([
            'success' => 'data_destroyed',
            'message' => 'Registro eliminado',
        ]);
    }
}
