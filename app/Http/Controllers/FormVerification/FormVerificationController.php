<?php

namespace App\Http\Controllers\FormVerification;

use App\Http\Controllers\Controller;
use App\Mail\FinishedRequestMail;
use App\Models\Admin\Family\Family;
use App\Models\Family\FormVerification\FormVerification;
use App\Models\GeneralResources\Schoolarship\Schoolarship;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Mail;

class FormVerificationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $formVerifications = FormVerification::all();

        return $formVerifications;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function finishedRequests($campus)
    {
        if ($campus == 0) {
            $finishedRequests = FormVerification::where('finished', 1)->with('family', 'schoolarship', 'student.campus')->get();

            return $finishedRequests;
        } else {
            $campusStudent = function ($q) use ($campus) {
                $q->where('campus_id', $campus);
            };

            $finishedRequests = FormVerification::where('finished', 1)->with(['family', 'schoolarship', 'student.campus', 'student' => $campusStudent])
                ->whereHas('student', $campusStudent)->get();

            return $finishedRequests;
        }

    }

    public function startedRequests()
    {
        $startedRequests = FormVerification::where('finished', 0)->with('family', 'schoolarship', 'student.campus')->get();

        return $startedRequests;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function familiesForResearch()
    {
        $familiesForResearch = FormVerification::where('finished', 1)->where('schoolarship_id', 1)->with(['family', 'family.user', 'family.company'])->get();

        return $familiesForResearch;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'family_id' => 'integer|required',
            'schoolarship_id' => 'integer|required',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $formVerification = FormVerification::create([
            'family_id' => $request->family_id,
            'schoolarship_id' => $request->schoolarship_id,
            'family' => $request->family,
            'student' => $request->student,
            'heritage_assets' => $request->heritage_assets,
            'monthly_expenses' => $request->monthly_expenses,
            'monthly_income' => $request->monthly_income,
            'schoolarship_reason' => $request->schoolarship_reason,
            'finished' => $request->finished,
        ]);

        return response()->json($formVerification);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Family\FormVerification\FormVerification  $formVerification
     * @return \Illuminate\Http\Response
     */
    public function show($familyId)
    {
        $formVerificationOfFamily = FormVerification::where('family_id', $familyId)->get();

        return $formVerificationOfFamily;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Models\Family\FormVerification\FormVerification  $formVerification
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $familyId)
    {
        $data = FormVerification::where('family_id', $familyId)->first();

        if ($request->schoolarship_id) {
            $data->schoolarship_id = $request->schoolarship_id;
        }
        if ($request->family) {
            $data->family = $request->family;
        }
        if ($request->student) {
            $data->student = $request->student;
        }
        if ($request->heritage_assets) {
            $data->heritage_assets = $request->heritage_assets;
        }
        if ($request->monthly_income) {
            $data->monthly_income = $request->monthly_income;
        }
        if ($request->monthly_expenses) {
            $data->monthly_expenses = $request->monthly_expenses;
        }
        if ($request->schoolarship_reason) {
            $data->schoolarship_reason = $request->schoolarship_reason;
        }
        if ($request->finished) {
            $data->finished = $request->finished;
            $data->save();

            //Get family by family_id
            $family = Family::where('family_id', $data->family_id)->first();
            $schoolarship = Schoolarship::find($data->schoolarship_id);

            //Mail Attributes
            $mailAttributes = [
                'family_name' => $family->ft_lastname.' '.$family->mt_lastname,
                'schoolarship' => $schoolarship->name,
                'family_id' => $data->family_id,
                'schoolarship_id' => $data->schoolarship_id,
            ];

            if ($family->ft_email && ! $family->mt_email) {
                $sendmail = Mail::to($family->ft_email)
                    ->bcc([env('MAIL_SYSTEMS_AREA'), env('MAIL_SCHOOLARSHIP')])
                    ->send(new FinishedRequestMail($mailAttributes));
                if (empty($sendmail)) {
                    return response()->json([
                        'data' => $data,
                        'message' => 'Correo enviado.',
                    ], 200);
                } else {
                    return response()->json(['message' => 'Falla en envío de correo.'], 400);
                }

            } elseif (! $family->ft_email && $family->mt_email) {
                $sendmail = Mail::to($family->mt_email)
                    ->bcc([env('MAIL_SYSTEMS_AREA'), env('MAIL_SCHOOLARSHIP')])
                    ->send(new FinishedRequestMail($mailAttributes));
                if (empty($sendmail)) {
                    return response()->json([
                        'data' => $data,
                        'message' => 'Correo enviado.',
                    ], 200);
                } else {
                    return response()->json(['message' => 'Falla en envío de correo.'], 400);
                }

            } elseif ($family->ft_email && $family->mt_email) {
                $sendmail = Mail::to([$family->ft_email, $family->mt_email])
                    ->bcc([env('MAIL_SYSTEMS_AREA'), env('MAIL_SCHOOLARSHIP')])
                    ->send(new FinishedRequestMail($mailAttributes));
                if (empty($sendmail)) {
                    return response()->json([
                        'data' => $data,
                        'message' => 'Correo enviado.',
                    ], 200);
                } else {
                    return response()->json(['message' => 'Falla en envío de correo.'], 400);
                }

            }
        }

        $data->save();

        return response()->json($data);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(FormVerification $formVerification)
    {
        $formVerification->delete();

        return response()->json([
            'success' => 'data_destroyed',
            'message' => 'Registro eliminado',
        ]);
    }
}
