<?php

namespace App\Http\Controllers\PasswordResetUserFamilyMail;

use App\Http\Controllers\Controller;
use App\Mail\PasswordResetFamilyMail;
use App\Models\Admin\UserFamily\UserFamily;
use App\Models\Family\PasswordResetUserFamily\PasswordResetUserFamily;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Mail;

class PasswordResetUserFamilyMailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function confirmation(Request $request)
    {

        $userWithFamily = UserFamily::with('family')->where('code', $request->code)->first();

        return $userWithFamily;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function sendPasswordResetMail(Request $request)
    {
        $userPassworReset = PasswordResetUserFamily::where('code', $request->code)->first();
        //Decrypt password
        $password = Crypt::decrypt($userPassworReset->password);

        $mailAttributes = [
            'code' => $request->code,
            'password' => $password,
        ];

        $sendmail = Mail::to($request->email)
            ->bcc([env('MAIL_SCHOOLARSHIP'), env('MAIL_SYSTEMS_AREA')])
            ->send(new PasswordResetFamilyMail($mailAttributes));

        if (empty($sendmail)) {
            return response()->json(['message' => 'Correo enviado.'], 200);
        } else {
            return response()->json(['message' => 'Falla en envío de correo.'], 400);
        }
    }
}
