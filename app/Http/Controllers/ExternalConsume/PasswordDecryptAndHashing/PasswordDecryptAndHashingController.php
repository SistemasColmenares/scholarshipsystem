<?php

namespace App\Http\Controllers\ExternalConsume\PasswordDecryptAndHashing;

use App\Http\Controllers\Controller;
use App\Models\Admin\UserFamily\UserFamily;
use App\Models\Family\PasswordResetUserFamily\PasswordResetUserFamily;
use Carbon\Carbon;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Hash;

class PasswordDecryptAndHashingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        //Method variables
        $today = Carbon::now()->toDateString();
        $userFamilies = UserFamily::whereDate('created_at', '=', $today)->get();
        // $userFamilies = UserFamily::whereBetween('created_at', [$yesterday, $today])->get();

        foreach ($userFamilies as $userFamily) {
            $option = substr($userFamily->code, 0, 1);
            // User colmenares
            if ($option == 'F') {
                $familyIdLenght = strlen($userFamily->family_id);
                if ($familyIdLenght == 1) {
                    $userFamily->code = 'F0000'.$userFamily->family_id;
                }
                if ($familyIdLenght == 2) {

                    $userFamily->code = 'F000'.$userFamily->family_id;
                }

                if ($familyIdLenght == 3) {
                    $userFamily->code = 'F00'.$userFamily->family_id;
                }

                if ($familyIdLenght == 4) {
                    $userFamily->code = 'F0'.$userFamily->family_id;
                }

                if ($familyIdLenght == 5) {
                    $userFamily->code = 'F'.$userFamily->family_id;
                }
                // User Los Altos
            } elseif ($option == 'L') {
                $familyId = substr($userFamily->code, 3);
                $familyIdLenght = strlen($familyId);

                if ($familyIdLenght == 1) {
                    $userFamily->code = 'LA_0000'.$familyId;
                }
                if ($familyIdLenght == 2) {

                    $userFamily->code = 'LA_000'.$familyId;
                }

                if ($familyIdLenght == 3) {
                    $userFamily->code = 'LA_00'.$familyId;
                }

                if ($familyIdLenght == 4) {
                    $userFamily->code = 'LA_0'.$familyId;
                }

                if ($familyIdLenght == 5) {
                    $userFamily->code = 'LA_'.$familyId;
                }
            }
            //Decrypt ASCII password
            $responseDecryptPassword = $this->decryptPassword($userFamily->family_id, $userFamily->password, 1);
            //Hash decrypt password
            $toEncryptPassword = $responseDecryptPassword;
            $responseDecryptPassword = Hash::make($responseDecryptPassword);
            //Update password
            $userFamily->password = $responseDecryptPassword;
            $userFamily->save();
            //Create data in password reset with crypt password
            $passwordReset = PasswordResetUserFamily::where('family_id', $userFamily->family_id)->first();
            if (! $passwordReset) {
                PasswordResetUserFamily::create([
                    'family_id' => $userFamily->family_id,
                    'code' => $userFamily->code,
                    'password' => Crypt::encrypt($toEncryptPassword),
                ]);
            }
        }

        return response()->json([
            'success' => 'process_complete',
            'message' => 'El proceso de homologación de usuarios, desencriptar y hashear contraseñas se completó.',
        ], 200);

    }

    public function decryptPassword($family, $password, $flag)
    {
        $option = fmod($family * 7, 3);

        switch ($option) {
            case 0:
                $arrayDecrypt[1] = -3;
                $arrayDecrypt[2] = -5;
                $arrayDecrypt[3] = -7;
                $arrayDecrypt[4] = 11;
                $arrayDecrypt[5] = 13;
                $arrayDecrypt[6] = 17;
                break;
            case 1:
                $arrayDecrypt[1] = 13;
                $arrayDecrypt[2] = -7;
                $arrayDecrypt[3] = 17;
                $arrayDecrypt[4] = -3;
                $arrayDecrypt[5] = 11;
                $arrayDecrypt[6] = -5;
                break;
            case 2:
                $arrayDecrypt[1] = -7;
                $arrayDecrypt[2] = 13;
                $arrayDecrypt[3] = -3;
                $arrayDecrypt[4] = 17;
                $arrayDecrypt[5] = -5;
                $arrayDecrypt[6] = 11;
                break;
        }

        if ($flag == 1) {
            for ($counter = 1; $counter <= 6; $counter++) {
                $arrayDecrypt[$counter] = -$arrayDecrypt[$counter];
            }
        }

        $decrypt = '';
        $answer = 1;
        do {
            $decrypt = $decrypt.chr(ord($password) + $arrayDecrypt[$answer]);
            $answer = $answer + 1;
            if ($answer > 6) {
                $answer = 1;
            }
            $password = substr($password, 1);
        } while (strlen($password) > 0);

        return $decrypt;
    }
}
