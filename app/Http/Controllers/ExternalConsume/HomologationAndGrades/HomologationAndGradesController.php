<?php

namespace App\Http\Controllers\ExternalConsume\HomologationAndGrades;

use App\Http\Controllers\Controller;
use App\Models\Admin\Student\Student;
use Carbon\Carbon;

class HomologationAndGradesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //Method variables
        $today = Carbon::now()->toDateString();
        //Homologation
        $studentsHomologation = Student::whereDate('updated_at', '=', $today)->where('la_status', '!=', null)->get();
        foreach ($studentsHomologation as $studentToHomologate) {
            $laStatus = substr($studentToHomologate->la_status, 0, 3);

            if ($laStatus == 'CO_') {
                $studentToHomologate->family_id = substr($studentToHomologate->la_status, 3);
                $studentToHomologate->save();
            } elseif ($laStatus == 'LA_') {
                $studentToHomologate->family_id = '900'.substr($studentToHomologate->la_status, 3);
                $studentToHomologate->save();
            }
        }
        //Update grades and delete students that exceed the grade of a section
        $studentsGrade = Student::whereIn('entry_status', ['S', 'N'])->whereDate('created_at', '=', $today)->get();
        //   $studentsGrade = Student::whereIn('entry_status', array('S', 'N'))->whereBetween('created_at', [$yesterday, $today])->get();
        foreach ($studentsGrade as $studentToGrade) {
            //Grade exceed to delete
            if ($studentToGrade->section == .5 && $studentToGrade->grade + 1 > 5) {
                $studentToGrade->delete();
            } elseif ($studentToGrade->section == 1 && $studentToGrade->grade + 1 > 6) {
                $studentToGrade->grade = 1;
                $studentToGrade->group = '';
                $studentToGrade->section = 2;
                $studentToGrade->save();
            } elseif ($studentToGrade->section == 2 && $studentToGrade->grade + 1 > 3) {
                $studentToGrade->grade = 1;
                $studentToGrade->group = '';
                $studentToGrade->section = 3;
                $studentToGrade->save();
            } elseif ($studentToGrade->section == 3 && $studentToGrade->grade + 2 > 6) {
                $studentToGrade->delete();
            } else {
                //Grade not exceed add one to grade if the section is not 3, else add two to grade
                $studentToGrade->grade = $studentToGrade->section != 3 ? $studentToGrade->grade + 1 : $studentToGrade->grade + 2;
                $studentToGrade->save();
            }
        }

        return response()->json([
            'success' => 'process_complete',
            'message' => 'El proceso de homologación y control de alumnos.',
        ], 200);
    }
}
