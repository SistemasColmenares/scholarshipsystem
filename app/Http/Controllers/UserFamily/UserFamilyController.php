<?php

namespace App\Http\Controllers\UserFamily;

use App\Http\Controllers\Controller;
use App\Models\Admin\AccountStatement\AccountStatement;
use App\Models\Admin\AfnImport\AfnImport;
use App\Models\Admin\Agreement\Agreement;
use App\Models\Admin\Charge\Charge;
use App\Models\Admin\ExternalPromissoryNote\ExternalPromissoryNote;
use App\Models\Admin\Payment\Payment;
use App\Models\Admin\Settings\Settings;
use App\Models\Admin\Student\Student;
use App\Models\Admin\UserFamily\UserFamily;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use JWTAuth;

class UserFamilyController extends Controller
{
    public function __construct()
    {
        Config::set('jwt.user', UserFamily::class);
        Config::set('auth.providers.users.model', UserFamily::class);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $userFamilies = UserFamily::all();

        return $userFamilies;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function accessOfFamilies($campus)
    {
        if ($campus == 0) {
            $userFamilies = UserFamily::with('formVerification')->get();

            return $userFamilies;
        } else {
            $campusStudent = function ($q) use ($campus) {
                $q->where('campus_id', $campus);
            };

            $userFamilies = UserFamily::with(['formVerification', 'student' => $campusStudent])
                ->whereHas('student', $campusStudent)->get();

            return $userFamilies;
        }
    }

    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show(UserFamily $userFamily)
    {
        return $userFamily;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Models\Admin\UserCompany\UserFamily  $userFamily
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $userFamily)
    {
        $data = UserFamily::find($userFamily);

        if ($request->individual_dates) {
            $data->individual_dates = $request->individual_dates;
            $data->start_date = $request->start_date;
            $data->end_date = $request->end_date;
        } else {
            $data->individual_dates = 0;
            $data->start_date = null;
            $data->end_date = null;
        }

        $data->full_access = $request->full_access;
        $data->observation = $request->observation ? $request->observation : null;

        $data->save();

        return response()->json($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Models\User\User  $user
     * @return \Illuminate\Http\Response
     */
    public function password(Request $request, $family)
    {

        $data = UserFamily::where('family_id', $family)->first();

        $validator = Validator::make($request->all(), [
            'password' => 'string|min:6|confirmed',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $data->password = Hash::make($request->password);

        $data->save();

        return response()->json($data);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Models\Admin\UserCompany\UserFamily  $userFamily
     * @return \Illuminate\Http\Response
     */
    public function updateTermsAndConditions(Request $request, $family)
    {
        $data = UserFamily::where('family_id', $family)->first();

        $validator = Validator::make($request->all(), [
            'terms_and_conditions' => 'required|max:255',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $data->terms_and_conditions = $request->terms_and_conditions;

        $data->save();

        return response()->json($data);
    }

    public function authenticate(Request $request)
    {
        //Mehod variables
        $actualDate = Carbon::now()->toDateString();
        $settings = Settings::first();
        $user = UserFamily::where('code', $request->get('code'))->first();

        // Date validation
        if ($settings->start_date <= $actualDate && $settings->end_date >= $actualDate) {
            //Full access user validation
            if ($user->full_access == 1) {
                return $this->goAuthenticate($request, $user);
            }

            //Schoolarship validations

            //Schoolarship: Interna/Oficial
            if ($request->schoolarship_id == 1) {
                // Check inscription
                $responseInscription = $this->inscription($user->family_id, 1);
                if ($responseInscription == 1) {
                    return response()->json([
                        'error' => 'invalid_inscription',
                        'message' => 'No se ha cubierto el porcentaje solicitado de inscripción.',
                    ], 400);
                }
                //Check charges and payments
                $responseChekChargesPayments = $this->chargesAndPayments($user->family_id, 1);
                if ($responseChekChargesPayments == 1) {
                    return response()->json([
                        'error' => 'invalid_charges_and_payments',
                        'message' => 'No se ha cubierto el total de los adeudos, favor de revisar con su colegio.',
                    ], 400);
                }

                //All good go authenticate
                return $this->goAuthenticate($request, $user);

                //Schoolarship: AFN
            } elseif ($request->schoolarship_id == 2) {
                // Check inscription
                $responseInscription = $this->inscription($user->family_id, 2);
                if ($responseInscription == 1) {
                    return response()->json([
                        'error' => 'invalid_inscription',
                        'message' => 'No se ha cubierto el porcentaje solicitado de inscripción.',
                    ], 400);
                }
                //Check charges and payments
                $responseChekChargesPayments = $this->chargesAndPayments($user->family_id, 2);
                if ($responseChekChargesPayments == 1) {
                    return response()->json([
                        'error' => 'invalid_charges_and_payments',
                        'message' => 'No se ha cubierto el total de los adeudos, favor de revisar con su colegio.',
                    ], 400);
                }

                //All good go authenticate
                return $this->goAuthenticate($request, $user);

                //Schoolarship: Orfandad
            } elseif ($request->schoolarship_id == 3) {
                //Check charges and payments
                $responseChekChargesPayments = $this->chargesAndPayments($user->family_id, 3);
                if ($responseChekChargesPayments == 1) {
                    return response()->json([
                        'error' => 'invalid_charges_and_payments',
                        'message' => 'No se ha cubierto el total de los adeudos, favor de revisar con su colegio.',
                    ], 400);
                }

                //All good go authenticate
                return $this->goAuthenticate($request, $user);
            }
            //User individual dates validation
        } elseif ($user->individual_dates) {
            if ($user->start_date <= $actualDate && $user->end_date >= $actualDate) {
                //Full access user validation
                if ($user->full_access == 1) {
                    return $this->goAuthenticate($request, $user);
                }

                //Schoolarship validations

                //Schoolarship: Interna/Oficial
                if ($request->schoolarship_id == 1) {
                    // Check inscription
                    $responseInscription = $this->inscription($user->family_id, 1);
                    if ($responseInscription == 1) {
                        return response()->json([
                            'error' => 'invalid_inscription',
                            'message' => 'No se ha cubierto el porcentaje solicitado de inscripción.',
                        ], 400);
                    }
                    //Check charges and payments
                    $responseChekChargesPayments = $this->chargesAndPayments($user->family_id, 1);
                    if ($responseChekChargesPayments == 1) {
                        return response()->json([
                            'error' => 'invalid_charges_and_payments',
                            'message' => 'No se ha cubierto el total de los adeudos, favor de revisar con su colegio.',
                        ], 400);
                    }

                    //All good go authenticate
                    return $this->goAuthenticate($request, $user);

                    //Schoolarship: AFN
                } elseif ($request->schoolarship_id == 2) {
                    // Check inscription
                    $responseInscription = $this->inscription($user->family_id, 2);
                    if ($responseInscription == 1) {
                        return response()->json([
                            'error' => 'invalid_inscription',
                            'message' => 'No se ha cubierto el porcentaje solicitado de inscripción.',
                        ], 400);
                    }
                    //Check charges and payments
                    $responseChekChargesPayments = $this->chargesAndPayments($user->family_id, 2);
                    if ($responseChekChargesPayments == 1) {
                        return response()->json([
                            'error' => 'invalid_charges_and_payments',
                            'message' => 'No se ha cubierto el total de los adeudos, favor de revisar con su colegio.',
                        ], 400);
                    }

                    //All good go authenticate
                    return $this->goAuthenticate($request, $user);

                    //Schoolarship: Orfandad
                } elseif ($request->schoolarship_id == 3) {
                    //Check charges and payments
                    $responseChekChargesPayments = $this->chargesAndPayments($user->family_id, 3);
                    if ($responseChekChargesPayments == 1) {
                        return response()->json([
                            'error' => 'invalid_charges_and_payments',
                            'message' => 'No se ha cubierto el total de los adeudos, favor de revisar con su colegio.',
                        ], 400);
                    }

                    //All good go authenticate
                    return $this->goAuthenticate($request, $user);
                }
            } else {
                return response()->json([
                    'error' => 'invalid_dates',
                    'message' => 'Fuera del periodo de solicitudes.',
                ], 400);
            }
        } else {
            return response()->json([
                'error' => 'invalid_dates',
                'message' => 'Fuera del periodo de solicitudes.',
            ], 400);
        }

    }

    // Support authenticate methods

    public function inscription($family_id, $schoolarship_id)
    {
        //Method variables
        $date = Carbon::now();
        $settings = Settings::first();
        $students = Student::where('family_id', $family_id)->get();

        //Scholarship: Interna/Oficial
        if ($schoolarship_id == 1) {
            foreach ($students as $student) {
                //Campus : Los Altos
                if ($student->campus_id == 2) {
                    // Get charges
                    $rowCharges = Charge::where('student', $student->register)->where('pay', '!=', 'S')->whereIn('month_pay', [8, 9, 10, 11, 12, 1, 2, 3])
                        ->whereNotIn('concept', [210, 220, 230, 303, 332, 333])->where('period', $date->year)->count();
                    if ($rowCharges == 0) {
                        $returnFlag = 0;
                    } else {
                        //Get payments
                        $paymentsImport = Payment::where('student', $student->register)->whereIn('concept', [210, 220, 230, 303, 332, 333])
                            ->where('period', $date->year)->sum('import');
                        if ($paymentsImport >= $inscriptionPercentPayed) {
                            $returnFlag = 0;
                        } else {
                            $chargesImport = Charge::where('student', $student->register)->whereIn('concept', [210, 220, 230, 303, 332, 333])->where('period', $date->year)->sum('import');
                            $inscriptionPercentPayed = $chargesImport * ($settings->inscription_percent / 100);
                            if ($chargesImport >= $inscriptionPercentPayed) {
                                $returnFlag = 0;
                            } else {
                                $responseEPN = $this->externalPromissoryNotes($student->register);
                                if ($responseLetter == 1) {
                                    $returnFlag = 0;
                                } else {
                                    return 1;
                                }
                            }
                        }
                    }
                    //Campus: Colmenares
                } else {
                    $rowAccountStatements = AccountStatement::where('schoolar_cycle', $date->year)->where('paid', '!=', 'S')->where('student_id', $student->register)
                        ->whereIn('concept_id', [1, 2, 3, 4, 16])->count();

                    if ($rowAccountStatements == 0) {
                        $returnFlag = 0;
                    } else {
                        $chargesImport = Charge::where('student', $student->register)->whereIn('concept', [1, 2, 3, 4, 16])->where('period', $date->year)->sum('import');
                        if (! $chargesImport) {
                            return 1;
                        }
                        $debitAccountStatements = AccountStatement::where('schoolar_cycle', $date->year)->where('paid', '!=', 'S')->where('student_id', $student->register)
                            ->whereIn('concept_id', [1, 2, 3, 4, 16])->sum('debit');
                        $inscriptionPercentPayed = $chargesImport * ($settings->inscription_percent / 100);
                        //Sub of $inscriptionPercentPayed - $debitAccountStatements
                        $payed = $inscriptionPercentPayed - $debitAccountStatements;
                        if ($payed >= $inscriptionPercentPayed) {
                            $returnFlag = 0;
                        } else {
                            $responseLetter = $this->letter($student->register);
                            if ($responseLetter == 1) {
                                $returnFlag = 0;
                            }

                            return 1;
                        }
                    }
                }
            }
            //Scholarship: AFN
        } elseif ($schoolarship_id == 2) {
            // Get the number of students
            $studentsCount = $students->count();
            if ($studentsCount < 3) {
                $schoolarshipPercent = 1;
            } elseif ($studentsCount == 3) {
                $schoolarshipPercent = .90;
            } elseif ($studentsCount >= 4) {
                $schoolarshipPercent = .75;
            }
            foreach ($students as $student) {
                $afnImports = AfnImport::where('student', $student->register)->first();
                //Campus : Los Altos
                if ($student->campus_id == 2) {
                    // Get payments
                    $paymentsImport = Payment::where('student', $student->register)->whereIn('concept', [210, 220, 230, 303, 332, 333])
                        ->where('period', $date->year)->sum('import');
                    if (! $paymentsImport) {
                        return 1;
                    }
                    if ($paymentsImport >= $afnImports->import) {
                        $returnFlag = 0;
                    } else {
                        $inscriptionPercentPayed = $paymentsImport * $schoolarshipPercent;
                        if ($inscriptionPercentPayed >= $paymentsImport) {
                            $returnFlag = 0;
                        } else {
                            //Get charges
                            $chargesImport = Charge::where('student', $student->register)->whereIn('concept', [210, 220, 230, 303, 332, 333])->sum('concept_import');
                            $inscriptionPercentPayed = $chargesImport * ($settings->inscription_percent / 100);
                            if ($paymentsImport >= $inscriptionPercentPayed) {
                                $returnFlag = 0;
                            } else {
                                //Get promissory notes of Los Altos
                                $responseEPN = $this->externalPromissoryNotes($student);
                                if ($responseEPN == 1) {
                                    $returnFlag = 0;
                                }

                                return 1;
                            }
                        }
                    }
                    //Campus: Colmenares
                } else {
                    $rowAccountStatements = AccountStatement::where('schoolar_cycle', $date->year)->where('paid', '!=', 'S')->where('student_id', $student->register)
                        ->whereIn('concept_id', [1, 2, 3, 4, 16])->count();
                    if ($rowAccountStatements == 0) {
                        $returnFlag = 0;
                    } else {
                        $paymentsImport = Payment::where('student', $student->register)->whereIn('concept', [1, 2, 3, 4, 16])
                            ->where('period', $date->year)->sum('import');
                        if ($paymentsImport >= $afnImports->import) {
                            $returnFlag = 0;
                        } else {
                            $inscriptionPercentPayed = $afnImports->import_month * $schoolarshipPercent;
                            if ($paymentsImport >= $inscriptionPercentPayed) {
                                $returnFlag = 0;
                            } else {
                                //Get charges
                                $chargesImport = Charge::where('student', $student->register)->whereIn('concept', [1, 2, 3, 4, 16])->sum('concept_import');
                                $inscriptionPercentPayed = $chargesImport * ($settings->inscription_percent / 100);
                                if ($paymentsImport >= $inscriptionPercentPayed) {
                                    $returnFlag = 0;
                                } else {
                                    $responseLetter = $this->letter($student->register);
                                    if ($responseLetter == 1) {
                                        $returnFlag = 0;
                                    }

                                    return 1;
                                }
                            }
                        }

                    }
                }
            }
        }
        if ($returnFlag == 0) {
            return 0;
        }
    }

    public function chargesAndPayments($family_id, $schoolarship_id)
    {
        //Method variables
        $date = Carbon::now();
        $settings = Settings::first();
        $students = Student::where('family_id', $family_id)->get();
        //Schoolarship: Interna/Oficial
        if ($schoolarship_id == 1) {
            foreach ($students as $student) {
                $responseDebit = $this->debit($student->register, $student->family_id, $student->campus_id);
                if ($responseDebit == 1) {
                    return 1;
                }
                $returnFlag = 0;
            }
            //Schoolarship: AFN
        } elseif ($schoolarship_id == 2) {
            foreach ($students as $student) {
                $responseDebit = $this->debit($student->register, $student->family_id, $student->campus_id);
                if ($responseDebit == 1) {
                    return 1;
                }
                $returnFlag = 0;
            }
            //Schoolarship: Orfandad
        } elseif ($schoolarship_id == 3) {
            foreach ($students as $student) {
                $responseDebit = $this->debit($student->register, $student->family_id, $student->campus_id);
                if ($responseDebit == 1) {
                    return 1;
                }
                $returnFlag = 0;
            }
        }

        if ($returnFlag == 0) {
            return 0;
        }

    }

    public function debit($student, $family_id, $campus)
    {
        //Method variables
        $date = Carbon::now();
        $lastYear = $date->subYear();
        $lastYear = $lastYear->format('Y');
        $students = Student::where('family_id', $family_id)->get();

        //Campus: Los Altos
        if ($campus == 2) {
            // Get charges
            $charges = Charge::where('student', $student)->where('pay', '!=', 'S')->where('pay_period', '<', $date->year)->whereIn('month_pay', [8, 9, 10, 11, 12, 1, 2, 3])
                ->whereNotIn('concept', [210, 220, 230, 303, 332, 333])->whereBetween('period', [2014, $lastYear])->sum('import');
            //Get payments
            $payments = Payment::where('student', $student)->whereIn('month_pay', [8, 9, 10, 11, 12, 1, 2, 3])
                ->whereNotIn('concept', [210, 220, 230, 303, 332, 333])->whereBetween('period', [2014, $lastYear])->sum('import');
            //Compare if payments is >= charges
            if ($payments >= $charges) {
                return 0;
            } else {
                //Get promissory notes of Los Altos
                $responseEPN = $this->externalPromissoryNotes($student);
                if ($responseEPN == 1) {
                    return 0;
                }

                return 1;
            }
            //Campus: Colmenares
        } else {
            // Get charges
            $charges = Charge::where('student', $student)->where('pay', '!=', 'S')->where('pay_period', '<', $date->year)->whereIn('month_pay', [8, 9, 10, 11, 12, 1, 2, 3])
                ->whereNotIn('concept', [1, 2, 3, 4, 16])->whereBetween('period', [2014, $lastYear])->sum('import');
            //Get payments
            $payments = Payment::where('student', $student)->whereIn('month_pay', [8, 9, 10, 11, 12, 1, 2, 3])
                ->whereNotIn('concept', [1, 2, 3, 4, 16])->whereBetween('period', [2014, $lastYear])->sum('import');
            //Compare if payments is >= charges
            if ($payments >= $charges) {
                return 0;
            } else {
                //Get account statements with agreements
                $responseAS = $this->accountStatements($student);
                dd($responseAS);
                if ($responseAS == 1) {
                    return 0;
                }

                return 1;
            }
        }
    }

    public function accountStatements($student)
    {
        //Method variables
        $date = Carbon::now();
        $accountStatements = AccountStatement::where('student_id', $student)->whereIn('month', [8, 9, 10, 11, 12, 1, 2, 3])->where('paid', '!=', 'S')
            ->whereNotIn('concept', [1, 2, 3, 4, 16])->whereBetween('schoolar_cycle', [2014, $date->year])->orderBy('payment_date', 'DESC')->get();

        if ($accountStatements) {
            foreach ($accountStatements as $accountStatement) {
                if (! $accountStatement->agreement_folio) {
                    return 0;
                } else {

                    $responseAgreements = $this->agreements($student, $accountStatement->agreement_folio);
                    if ($responseAgreements) {
                        return 1;
                    }
                }
            }

        } else {
            return 1;
        }

    }

    public function agreements($student, $agreement)
    {
        $agreements = Agreement::where('agreement_folio', $agreement)->where('student', $student)->get();
        if ($agreements) {
            return 1;
        }

        return 0;
    }

    public function externalPromissoryNotes($student)
    {
        $responseEPN = ExternalPromissoryNote::where('register', $student);
        if ($responseEPN) {
            return 1;
        }

        return 0;
    }

    public function letter($register)
    {
        //Method variables
        $student = Student::where('register', $register)->first();

        if ($student->letter == 1) {
            return 1;
        }

        return 0;
    }

    // JWT authentication
    public function goAuthenticate($request, $user)
    {
        // Grab credentials from the request

        $credentials = $request->only('code', 'password');

        try {

            // Attempt to verify the credentials and create a token for the user
            if (! $token = JWTAuth::attempt($credentials)) {
                return response()->json([
                    'error' => 'invalid_credentials',
                    'message' => 'Las credenciales ingresadas no son correctas.',
                ], 401);
            }
        } catch (JWTException $e) {
            // Something went wrong whilst attempting to encode the token
            return response()->json([
                'error' => 'could_not_create_token',
                'message' => 'Servicio no disponbile por el momento.',
            ], 422);
        }

        // all good so return the token

        return response()->json([
            'user' => $user,
            'token' => $token,
        ], 200);
    }
}
