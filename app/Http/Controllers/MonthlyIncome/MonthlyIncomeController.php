<?php

namespace App\Http\Controllers\MonthlyIncome;

use App\Http\Controllers\Controller;
use App\Models\Family\MonthlyIncome\MonthlyIncome;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class MonthlyIncomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $monthlyIncomes = MonthlyIncome::all();

        return $monthlyIncomes;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'family_id' => 'integer|required',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $monthlyIncome = MonthlyIncome::create([
            'family_id' => $request->family_id,
            'father' => $request->father,
            'mother' => $request->mother,
            'pantry_voucher' => $request->pantry_voucher,
            'other' => $request->other,
            'total' => $request->total,
        ]);

        return response()->json($monthlyIncome);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Family\MonthlyIncome\MonthlyIncome  $monthlyIncome
     * @return \Illuminate\Http\Response
     */
    public function show($family)
    {
        $monthlyIncomeOfFamily = MonthlyIncome::where('family_id', $family)->get();

        return $monthlyIncomeOfFamily;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Models\Family\MonthlyIncome\MonthlyIncome  $monthlyIncome
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $family)
    {
        $data = MonthlyIncome::where('family_id', $family)->first();

        $data->father = $request->father;
        $data->mother = $request->mother;
        $data->pantry_voucher = $request->pantry_voucher;
        $data->other = $request->other;
        $data->total = $request->total;

        $data->save();

        return response()->json($data);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(MonthlyIncome $monthlyIncome)
    {
        $monthlyIncome->delete();

        return response()->json([
            'success' => 'data_destroyed',
            'message' => 'Registro eliminado',
        ]);
    }
}
