<?php

namespace App\Http\Controllers\GeneralResources\BusinessActivity;

use App\Http\Controllers\Controller;
use App\Models\GeneralResources\BusinessActivity\BusinessActivity;

class BusinessActivityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $businessActivities = BusinessActivity::all();

        return $businessActivities;
    }

    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show(BusinessActivity $businessActivity)
    {
        return $businessActivity;
    }
}
