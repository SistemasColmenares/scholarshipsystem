<?php

namespace App\Http\Controllers\GeneralResources\UpStatus;

use App\Http\Controllers\Controller;
use App\Models\GeneralResources\UpStatus\UpStatus;

class UpStatusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $upStatuses = UpStatus::all();

        return $upStatuses;
    }

    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show(UpStatus $upStatus)
    {
        return $upStatus;
    }
}
