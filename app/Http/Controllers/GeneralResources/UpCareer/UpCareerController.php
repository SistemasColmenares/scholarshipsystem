<?php

namespace App\Http\Controllers\GeneralResources\UpCareer;

use App\Http\Controllers\Controller;
use App\Models\GeneralResources\UpCareer\UpCareer;

class UpCareerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $upCareers = UpCareer::all();

        return $upCareers;
    }

    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show(UpCareer $upCareer)
    {
        return $upCareer;
    }
}
