<?php

namespace App\Http\Controllers\GeneralResources\Profession;

use App\Http\Controllers\Controller;
use App\Models\GeneralResources\Profession\Profession;

class ProfessionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $professions = Profession::all();

        return $professions;
    }

    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Profession $profession)
    {
        return $profession;
    }
}
