<?php

namespace App\Http\Controllers\GeneralResources\Schoolarship;

use App\Http\Controllers\Controller;
use App\Models\GeneralResources\Schoolarship\Schoolarship;

class SchoolarshipController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $schoolarships = Schoolarship::all();

        return $schoolarships;
    }

    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Schoolarship $schoolarship)
    {
        return $schoolarship;
    }
}
