<?php

namespace App\Http\Controllers\GeneralResources\Campus;

use App\Http\Controllers\Controller;
use App\Models\GeneralResources\Campus\Campus;

class CampusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $campuses = Campus::all();

        return $campuses;
    }

    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Campus $campus)
    {
        return $campus;
    }
}
