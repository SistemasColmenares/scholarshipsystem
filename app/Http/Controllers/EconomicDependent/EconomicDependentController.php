<?php

namespace App\Http\Controllers\EconomicDependent;

use App\Http\Controllers\Controller;
use App\Models\Family\EconomicDependent\EconomicDependent;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class EconomicDependentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $economicDependents = EconomicDependent::all();

        return $economicDependents;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'family_id' => 'integer|required',
            'name' => 'required|string|max:255',
            'age' => 'required|string|max:255',
            'kinship' => 'required|string|max:255',
            'ocupation' => 'required|string|max:255',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $economicDependent = EconomicDependent::create([
            'family_id' => $request->family_id,
            'name' => $request->name,
            'age' => $request->age,
            'kinship' => $request->kinship,
            'ocupation' => $request->ocupation,
        ]);

        return response()->json($economicDependent);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Family\EconomicDependent\EconomicDependent  $economicDependent
     * @return \Illuminate\Http\Response
     */
    public function show($family)
    {
        $economicDependentOfFamily = EconomicDependent::where('family_id', $family)->get();

        return $economicDependentOfFamily;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(EconomicDependent $economicDependent)
    {
        $economicDependent->delete();

        return response()->json([
            'success' => 'data_destroyed',
            'message' => 'Registro eliminado',
        ]);
    }
}
