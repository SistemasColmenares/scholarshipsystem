<?php

namespace App\Http\Controllers\Role;

use App\Http\Controllers\Controller;
use App\Models\User\Role\Role;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $roles = Role::all();

        return $roles;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\User\Role\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function show($role)
    {
        $roleAndPermission = Role::with('permission')->find($role);

        return $roleAndPermission;
    }
}
