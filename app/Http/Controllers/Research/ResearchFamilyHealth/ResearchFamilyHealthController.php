<?php

namespace App\Http\Controllers\Research\ResearchFamilyHealth;

use App\Http\Controllers\Controller;
use App\Models\Research\ResearchFamilyHealth\ResearchFamilyHealth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ResearchFamilyHealthController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $researchFamilyHealths = ResearchFamilyHealth::all();

        return $researchFamilyHealths;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'family_id' => 'integer|required',
            'health_status' => 'required|string|max:255',
            'medical_service' => 'required|string|max:255',
            'assured' => 'required',
            'observation' => 'required|string',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        if (! $request->assured) {
            $assured = 0;
            $insurance_policy_cost = 0;
            $insured_amount = 0;
        } else {
            $assured = $request->assured;
            $insurance_policy_cost = $request->insurance_policy_cost;
            $insured_amount = $request->insurance_policy_cost;
        }

        $researchFamilyHealth = ResearchFamilyHealth::create([
            'family_id' => $request->family_id,
            'health_status' => $request->health_status,
            'member_disease' => $request->member_disease,
            'medical_service' => $request->medical_service,
            'assured' => $assured,
            'insurance_policy_cost' => $insurance_policy_cost,
            'insured_amount' => $insured_amount,
            'observation' => $request->observation,
        ]);

        return response()->json($researchFamilyHealth);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Research\ResearchFamilyHealth\ResearchFamilyHealth  $researchFamilyHealth
     * @return \Illuminate\Http\Response
     */
    public function show($familyId)
    {
        $researchFamilyHealthOfFamily = ResearchFamilyHealth::where('family_id', $familyId)->get();

        return $researchFamilyHealthOfFamily;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Models\Research\ResearchFamilyHealth\ResearchFamilyHealth  $researchFamilyHealth
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $familyId)
    {
        $data = ResearchFamilyHealth::where('family_id', $familyId)->first();

        if (! $request->assured) {
            $assured = 0;
            $insurance_policy_cost = 0;
            $insured_amount = 0;
        } else {
            $assured = $request->assured;
            $insurance_policy_cost = $request->insurance_policy_cost;
            $insured_amount = $request->insurance_policy_cost;
        }

        $data->health_status = $request->health_status;
        $data->member_disease = $request->member_disease;
        $data->medical_service = $request->medical_service;
        $data->assured = $assured;
        $data->insurance_policy_cost = $insurance_policy_cost;
        $data->insured_amount = $insured_amount;
        $data->observation = $request->observation;

        $data->save();

        return response()->json($data);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(ResearchFamilyHealth $researchFamilyHealth)
    {
        $researchFamilyHealth->delete();

        return response()->json([
            'success' => 'data_destroyed',
            'message' => 'Registro eliminado',
        ]);
    }
}
