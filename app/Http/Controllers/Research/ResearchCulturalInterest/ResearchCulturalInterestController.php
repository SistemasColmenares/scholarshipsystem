<?php

namespace App\Http\Controllers\Research\ResearchCulturalInterest;

use App\Http\Controllers\Controller;
use App\Models\Research\ResearchCulturalInterest\ResearchCulturalInterest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ResearchCulturalInterestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $researchCulturalInterests = ResearchCulturalInterest::all();

        return $researchCulturalInterests;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'family_id' => 'integer|required',
            'place' => 'required|string|max:255',
            'name' => 'required|string|max:255',
            'position' => 'required|string|max:255',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $researchCulturalInterest = ResearchCulturalInterest::create([
            'family_id' => $request->family_id,
            'place' => $request->place,
            'name' => $request->name,
            'position' => $request->position,
        ]);

        return response()->json($researchCulturalInterest);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Research\ResearchCulturalInterest\ResearchCulturalInterest  $researchCulturalInterest
     * @return \Illuminate\Http\Response
     */
    public function show($family)
    {
        $researchCulturalInterestOfFamily = ResearchCulturalInterest::where('family_id', $family)->get();

        return $researchCulturalInterestOfFamily;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(ResearchCulturalInterest $researchCulturalInterest)
    {
        $researchCulturalInterest->delete();

        return response()->json([
            'success' => 'data_destroyed',
            'message' => 'Registro eliminado',
        ]);
    }
}
