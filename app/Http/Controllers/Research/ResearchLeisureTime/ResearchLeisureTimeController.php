<?php

namespace App\Http\Controllers\Research\ResearchLeisureTime;

use App\Http\Controllers\Controller;
use App\Models\Research\ResearchLeisureTime\ResearchLeisureTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ResearchLeisureTimeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $researchLeisureTimes = ResearchLeisureTime::all();

        return $researchLeisureTimes;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'family_id' => 'integer|required',
            'free_time' => 'required|string|max:255',
            'free_time_frequency' => 'required|string|max:255',
            'free_time_place' => 'required|string|max:255',
            'vacation_frequency' => 'required|string|max:255',
            'vacation_place' => 'required|string|max:255',
            'last_vacation' => 'required|string|max:255',
            'observation' => 'required|string',

        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $researchLeisureTime = ResearchLeisureTime::create([
            'family_id' => $request->family_id,
            'free_time' => $request->free_time,
            'free_time_frequency' => $request->free_time_frequency,
            'free_time_place' => $request->free_time_place,
            'vacation_frequency' => $request->vacation_frequency,
            'vacation_place' => $request->vacation_place,
            'last_vacation' => $request->last_vacation,
            'observation' => $request->observation,
        ]);

        return response()->json($researchLeisureTime);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Research\ResearchLeisureTime\ResearchLeisureTime  $researchLeisureTime
     * @return \Illuminate\Http\Response
     */
    public function show($familyId)
    {
        $researchLeisureTimeOfFamily = ResearchLeisureTime::where('family_id', $familyId)->get();

        return $researchLeisureTimeOfFamily;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Models\Research\ResearchLeisureTime\ResearchLeisureTime  $researchLeisureTime
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $familyId)
    {
        $data = ResearchLeisureTime::where('family_id', $familyId)->first();

        $data->free_time = $request->free_time;
        $data->free_time_frequency = $request->free_time_frequency;
        $data->free_time_place = $request->free_time_place;
        $data->vacation_frequency = $request->vacation_frequency;
        $data->vacation_place = $request->vacation_place;
        $data->last_vacation = $request->last_vacation;
        $data->observation = $request->observation;

        $data->save();

        return response()->json($data);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(ResearchLeisureTime $researchLeisureTime)
    {
        $researchFamilyHealth->delete();

        return response()->json([
            'success' => 'data_destroyed',
            'message' => 'Registro eliminado',
        ]);
    }
}
