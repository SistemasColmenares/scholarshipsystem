<?php

namespace App\Http\Controllers\Research\ResearchReference;

use App\Http\Controllers\Controller;
use App\Models\Research\ResearchReference\ResearchReference;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ResearchReferenceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $researchCulturalReferences = ResearchReference::all();

        return $researchCulturalReferences;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'family_id' => 'integer|required',
            'name' => 'required|string|max:255',
            'phone' => 'required|string|max:255',
            'relationship' => 'required|string|max:255',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $researchCulturalReference = ResearchReference::create([
            'family_id' => $request->family_id,
            'name' => $request->name,
            'phone' => $request->phone,
            'relationship' => $request->relationship,
        ]);

        return response()->json($researchCulturalReference);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Research\ResearchReference\ResearchReference  $researchReference
     * @return \Illuminate\Http\Response
     */
    public function show($family)
    {
        $researchReferenceOfFamily = ResearchReference::where('family_id', $family)->get();

        return $researchReferenceOfFamily;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(ResearchReference $researchReference)
    {
        $researchReference->delete();

        return response()->json([
            'success' => 'data_destroyed',
            'message' => 'Registro eliminado',
        ]);
    }
}
