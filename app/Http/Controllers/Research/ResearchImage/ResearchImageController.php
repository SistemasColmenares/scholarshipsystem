<?php

namespace App\Http\Controllers\Research\ResearchImage;

use App\Http\Controllers\Controller;
use App\Models\Research\ResearchImage\ResearchImage;
use App\SupportFiles\FileManager;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class ResearchImageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $researchImages = ResearchImage::all();

        return $researchImages;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'family_id' => 'integer|required',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|dimensions:min_width=100,min_height=100',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        if ($request->hasFile('image')) {
            $image = FileManager::uploadImage($request->image, 600, 400);
        }

        $researchImage = ResearchImage::create([
            'family_id' => $request->family_id,
            'image' => $image->path,
            'image_name' => $image->name,
        ]);

        return response()->json($researchImage);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Research\ResearchImage\ResearchImage  $researchImage
     * @return \Illuminate\Http\Response
     */
    public function show($family)
    {
        $researchImageOfFamily = ResearchImage::where('family_id', $family)->get();

        return $researchImageOfFamily;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(ResearchImage $researchImage)
    {
        $imageToDelete = 'img/'.$researchImage->image_name.'.jpg';
        Storage::disk('public')->delete($imageToDelete);
        $researchImage->delete();

        return response()->json([
            'success' => 'data_destroyed',
            'message' => 'Registro eliminado.',
        ]);
    }
}
