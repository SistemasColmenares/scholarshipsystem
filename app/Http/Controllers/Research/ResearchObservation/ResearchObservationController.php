<?php

namespace App\Http\Controllers\Research\ResearchObservation;

use App\Http\Controllers\Controller;
use App\Models\Research\ResearchObservation\ResearchObservation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ResearchObservationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $researchObservations = ResearchObservation::all();

        return $researchObservations;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'family_id' => 'integer|required',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $researchObservation = ResearchObservation::create([
            'family_id' => $request->family_id,
            'family' => $request->family,
            'student' => $request->student,
            'economic_dependent' => $request->economic_dependent,
            'monthly_income' => $request->monthly_income,
            'monthly_expenses' => $request->monthly_expenses,
            'vehicle' => $request->vehicle,
        ]);

        return response()->json($researchObservation);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Research\ResearchObservation\ResearchObservation  $researchObservation
     * @return \Illuminate\Http\Response
     */
    public function show($familyId)
    {
        $researchObservationOfFamily = ResearchObservation::where('family_id', $familyId)->get();

        return $researchObservationOfFamily;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Models\Research\ResearchObservation\ResearchObservation  $researchObservation
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $familyId)
    {

        $data = ResearchObservation::where('family_id', $familyId)->first();

        if ($request->family) {
            $data->family = $request->family;
        }
        if ($request->student) {
            $data->student = $request->student;
        }
        if ($request->economic_dependent) {
            $data->economic_dependent = $request->economic_dependent;
        }
        if ($request->monthly_income) {
            $data->monthly_income = $request->monthly_income;
        }
        if ($request->monthly_expenses) {
            $data->monthly_expenses = $request->monthly_expenses;
        }
        if ($request->vehicle) {
            $data->vehicle = $request->vehicle;
        }

        $data->save();

        return response()->json($data);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(ResearchObservation $researchObservation)
    {
        $researchFormVerification->delete();

        return response()->json([
            'success' => 'data_destroyed',
            'message' => 'Registro eliminado',
        ]);
    }
}
