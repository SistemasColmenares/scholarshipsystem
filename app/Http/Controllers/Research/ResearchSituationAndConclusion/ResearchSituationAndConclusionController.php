<?php

namespace App\Http\Controllers\Research\ResearchSituationAndConclusion;

use App\Http\Controllers\Controller;
use App\Models\Research\ResearchSituationAndConclusion\ResearchSituationAndConclusion;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ResearchSituationAndConclusionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $researchSituationAndConclusions = ResearchSituationAndConclusion::all();

        return $researchSituationAndConclusions;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'family_id' => 'integer|required',
            'situation' => 'required|string|max:255',
            'conclusion' => 'required|string',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $researchSituationAndConclusion = ResearchSituationAndConclusion::create([
            'family_id' => $request->family_id,
            'situation' => $request->situation,
            'conclusion' => $request->conclusion,
        ]);

        return response()->json($researchSituationAndConclusion);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Research\ResearchSituationAndConclusion\ResearchSituationAndConclusion  $researchSituationAndConclusion
     * @return \Illuminate\Http\Response
     */
    public function show($familyId)
    {
        $researchSituationAndConclusionOfFamily = ResearchSituationAndConclusion::with('formVerification')->where('family_id', $familyId)->get();

        return $researchSituationAndConclusionOfFamily;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Models\Research\ResearchSituationAndConclusion\ResearchSituationAndConclusion  $researchSituationAndConclusion
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $familyId)
    {
        $data = ResearchSituationAndConclusion::where('family_id', $familyId)->first();

        $data->situation = $request->situation;
        $data->conclusion = $request->conclusion;
        $data->save();

        return response()->json($data);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(ResearchSituationAndConclusion $researchSituationAndConclusion)
    {
        $researchSituationAndConclusion->delete();

        return response()->json([
            'success' => 'data_destroyed',
            'message' => 'Registro eliminado',
        ]);
    }
}
