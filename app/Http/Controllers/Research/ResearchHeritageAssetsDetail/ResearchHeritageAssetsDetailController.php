<?php

namespace App\Http\Controllers\Research\ResearchHeritageAssetsDetail;

use App\Http\Controllers\Controller;
use App\Models\Research\ResearchHeritageAssetsDetail\ResearchHeritageAssetsDetail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ResearchHeritageAssetsDetailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $researchHeritageAssetsDetails = ResearchHeritageAssetsDetail::all();

        return $researchHeritageAssetsDetails;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'family_id' => 'integer|required',
            'property' => 'required|string|max:255',
            'location' => 'required|string|max:255',
            'property_price' => 'required',
            'owner' => 'required|string|max:255',
            'permanence' => 'required|integer',
            'people' => 'required|integer',
            'property_type' => 'required|string|max:255',
            'property_status' => 'required|string|max:255',
            'surface' => 'required',
            'building' => 'required',
            'building_price' => 'required',
            'added_value' => 'required|string|max:255',
            'bedroom' => 'required|integer',
            'bathroom' => 'required|integer',
            'living_room' => 'required|integer',
            'dinning_room' => 'required|integer',
            'yard' => 'required|integer',
            'tv' => 'required|integer',
            'computer' => 'required|integer',
            'dvd_bluray_videogame' => 'required|integer',
            'smartdevice' => 'required|integer',
            'refrigerator' => 'required|integer',
            'cellphone_tablet' => 'required|integer',
            'gardener' => 'required|integer',
            'domestic_employee' => 'required|integer',
            'driver' => 'required|integer',
            'kitchener' => 'required|integer',
            'cable_phone_internet' => 'required|integer',
            'streaming_service' => 'required|integer',
            'general_conditions' => 'required|string',
            'observation' => 'required|string',

        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $researchHeritageAssetsDetail = ResearchHeritageAssetsDetail::create([
            'family_id' => $request->family_id,
            'property' => $request->property,
            'location' => $request->location,
            'property_price' => $request->property_price,
            'owner' => $request->owner,
            'permanence' => $request->permanence,
            'people' => $request->people,
            'property_type' => $request->property_type,
            'property_status' => $request->property_status,
            'surface' => $request->surface,
            'building' => $request->building,
            'building_price' => $request->building_price,
            'added_value' => $request->added_value,
            'bedroom' => $request->bedroom,
            'bathroom' => $request->bathroom,
            'living_room' => $request->living_room,
            'dinning_room' => $request->dinning_room,
            'yard' => $request->yard,
            'tv' => $request->tv,
            'computer' => $request->computer,
            'dvd_bluray_videogame' => $request->dvd_bluray_videogame,
            'smartdevice' => $request->smartdevice,
            'refrigerator' => $request->refrigerator,
            'cellphone_tablet' => $request->cellphone_tablet,
            'gardener' => $request->gardener,
            'domestic_employee' => $request->domestic_employee,
            'driver' => $request->driver,
            'kitchener' => $request->kitchener,
            'cable_phone_internet' => $request->cable_phone_internet,
            'streaming_service' => $request->streaming_service,
            'general_conditions' => $request->general_conditions,
            'observation' => $request->observation,
        ]);

        return response()->json($researchHeritageAssetsDetail);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Research\ResearchHeritageAssetsDetail\ResearchHeritageAssetsDetail  $researchHeritageAssetsDetail
     * @return \Illuminate\Http\Response
     */
    public function show($familyId)
    {
        $researchHeritageAssetsDetailOfFamily = ResearchHeritageAssetsDetail::where('family_id', $familyId)->get();

        return $researchHeritageAssetsDetailOfFamily;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Models\Research\ResearchHeritageAssetsDetail\ResearchHeritageAssetsDetail  $researchHeritageAssetsDetail
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $familyId)
    {
        $data = ResearchHeritageAssetsDetail::where('family_id', $familyId)->first();

        $data->property = $request->property;
        $data->location = $request->location;
        $data->property_price = $request->property_price;
        $data->owner = $request->owner;
        $data->permanence = $request->permanence;
        $data->people = $request->people;
        $data->property_type = $request->property_type;
        $data->property_status = $request->property_status;
        $data->surface = $request->surface;
        $data->building = $request->building;
        $data->building_price = $request->building_price;
        $data->added_value = $request->added_value;
        $data->bedroom = $request->bedroom;
        $data->bathroom = $request->bathroom;
        $data->living_room = $request->living_room;
        $data->dinning_room = $request->dinning_room;
        $data->yard = $request->yard;
        $data->tv = $request->tv;
        $data->computer = $request->computer;
        $data->dvd_bluray_videogame = $request->dvd_bluray_videogame;
        $data->smartdevice = $request->smartdevice;
        $data->refrigerator = $request->refrigerator;
        $data->cellphone_tablet = $request->cellphone_tablet;
        $data->gardener = $request->gardener;
        $data->domestic_employee = $request->domestic_employee;
        $data->driver = $request->driver;
        $data->kitchener = $request->kitchener;
        $data->cable_phone_internet = $request->cable_phone_internet;
        $data->streaming_service = $request->streaming_service;
        $data->general_conditions = $request->general_conditions;
        $data->observation = $request->observation;
        $data->save();

        return response()->json($data);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(ResearchHeritageAssetsDetail $researchHeritageAssetsDetail)
    {
        $researchHeritageAssetsDetail->delete();

        return response()->json([
            'success' => 'data_destroyed',
            'message' => 'Registro eliminado',
        ]);
    }
}
