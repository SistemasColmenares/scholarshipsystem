<?php

namespace App\Http\Controllers\Research\ResearchFormVerification;

use App\Http\Controllers\Controller;
use App\Models\Research\ResearchFormVerification\ResearchFormVerification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ResearchFormVerificationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $researchFormVerifications = ResearchFormVerification::all();

        return $researchFormVerifications;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function finishedResearchsByCampus($campus)
    {
        if ($campus == 0) {
            $finishedResearchs = ResearchFormVerification::where('finished', 1)->with(['familyObject', 'familyObject.user', 'familyObject.company'])->get();

            return $finishedResearchs;
        } else {
            $campusStudent = function ($q) use ($campus) {
                $q->where('campus_id', $campus);
            };

            $finishedResearchs = ResearchFormVerification::where('finished', 1)->with(['familyObject', 'familyObject.user', 'familyObject.company'])->whereHas('student', $campusStudent)->get();

            return $finishedResearchs;
        }

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function finishedResearchsByCompany($company)
    {
        $finishedResearchs = ResearchFormVerification::where('finished', 1)
            ->where('company_id', $company)->with(['familyObject', 'familyObject.user', 'familyObject.company'])->get();

        return $finishedResearchs;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function toDoResearchs($company)
    {
        $toDoResearchs = ResearchFormVerification::where('finished', 0)
            ->where('company_id', $company)->with(['familyObject', 'familyObject.user'])->get();

        return $toDoResearchs;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'family_id' => 'integer|required',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $researchFormVerification = ResearchFormVerification::create([
            'family_id' => $request->family_id,
            'company_id' => $request->company_id,
            'family' => $request->family,
            'student' => $request->student,
            'economic_dependent' => $request->economic_dependent,
            'monthly_income' => $request->monthly_income,
            'monthly_expenses' => $request->monthly_expenses,
            'vehicle' => $request->vehicle,
            'heritage_assets_detail' => $request->heritage_assets_detail,
            'family_health' => $request->family_health,
            'leisure_time' => $request->leisure_time,
            'economic_situation_and_conclusion' => $request->economic_situation_and_conclusion,
            'photos' => $request->photos,
            'finished' => $request->finished,
        ]);

        return response()->json($researchFormVerification);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Research\ResearchFormVerification\ResearchFormVerification  $researchFormVerification
     * @return \Illuminate\Http\Response
     */
    public function show($familyId)
    {
        $researchFormVerificationOfFamily = ResearchFormVerification::where('family_id', $familyId)->first();

        return $researchFormVerificationOfFamily;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Models\Research\ResearchFormVerification\ResearchFormVerification  $researchFormVerification
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $familyId)
    {
        $data = ResearchFormVerification::where('family_id', $familyId)->first();

        if ($request->company_id) {

            $data->company_id = $request->company_id;
        }

        if ($request->family) {

            $data->family = $request->family;
        }
        if ($request->student) {
            $data->student = $request->student;
        }
        if ($request->economic_dependent) {
            $data->economic_dependent = $request->economic_dependent;
        }
        if ($request->monthly_income) {
            $data->monthly_income = $request->monthly_income;
        }
        if ($request->monthly_expenses) {
            $data->monthly_expenses = $request->monthly_expenses;
        }
        if ($request->vehicle) {
            $data->vehicle = $request->vehicle;
        }
        if ($request->heritage_assets_detail) {
            $data->heritage_assets_detail = $request->heritage_assets_detail;
        }
        if ($request->family_health) {
            $data->family_health = $request->family_health;
        }
        if ($request->leisure_time) {
            $data->leisure_time = $request->leisure_time;
        }
        if ($request->economic_situation_and_conclusion) {
            $data->economic_situation_and_conclusion = $request->economic_situation_and_conclusion;
        }
        if ($request->photos) {
            $data->photos = $request->photos;
        }
        if ($request->finished) {
            $data->finished = $request->finished;
        }

        $data->save();

        return response()->json($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Models\Research\ResearchFormVerification\ResearchFormVerification  $researchFormVerification
     * @return \Illuminate\Http\Response
     */
    public function updateCompanyId(Request $request, $familyId)
    {
        $data = ResearchFormVerification::where('family_id', $familyId)->first();

        $data->company_id = $request->company_id;

        $data->save();

        return response()->json($data);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(ResearchFormVerification $researchFormVerification)
    {
        $researchFormVerification->delete();

        return response()->json([
            'success' => 'data_destroyed',
            'message' => 'Registro eliminado',
        ]);
    }
}
