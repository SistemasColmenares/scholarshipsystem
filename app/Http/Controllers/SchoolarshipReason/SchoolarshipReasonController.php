<?php

namespace App\Http\Controllers\SchoolarshipReason;

use App\Http\Controllers\Controller;
use App\Models\Family\SchoolarshipReason\SchoolarshipReason;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class SchoolarshipReasonController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $schoolarshipReasons = SchoolarshipReason::all();

        return $schoolarshipReasons;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'family_id' => 'integer|required',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $schoolarshipReason = SchoolarshipReason::create([
            'family_id' => $request->family_id,
            'description' => $request->description,
        ]);

        return response()->json($schoolarshipReason);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Family\SchoolarshipReason\SchoolarshipReason  $schoolarshipReason
     * @return \Illuminate\Http\Response
     */
    public function show($family)
    {
        $schoolarshipReasonOfFamily = SchoolarshipReason::where('family_id', $family)->get();

        return $schoolarshipReasonOfFamily;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Models\Family\SchoolarshipReason\SchoolarshipReason  $schoolarshipReason
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $family)
    {
        $data = SchoolarshipReason::where('family_id', $family)->first();

        $data->description = $request->description;

        $data->save();

        return response()->json($data);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(SchoolarshipReason $schoolarshipReason)
    {
        $schoolarshipReason->delete();

        return response()->json([
            'success' => 'data_destroyed',
            'message' => 'Registro eliminado',
        ]);
    }
}
