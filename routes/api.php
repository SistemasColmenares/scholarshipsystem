<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */
/*
Route::middleware('auth:api')->get('/user', function (Request $request) {
return $request->user();
});
 */
//CORS
// Route::group(['middleware' => ['cors']], function () {
Route::group(['prefix' => 'auth'], function () {
    //Auth
    Route::post('/login', 'Auth\UserController@authenticate');
    Route::post('/register', 'Auth\UserController@register');
    //User
    Route::delete('/user/{user}', 'Auth\UserController@destroy');
    Route::get('/user', 'Auth\UserController@index');
    Route::get('/user/{user}', 'Auth\UserController@show');
    Route::put('/user/{user}', 'Auth\UserController@update');
    //User roles
    Route::get('/role', 'Role\RoleController@index');
    Route::get('/role/{role}', 'Role\RoleController@show');
    //Student
    Route::get('/student', 'Student\StudentController@index');
    Route::get('/student/{student}', 'Student\StudentController@show');
    Route::put('/student/{student}', 'Student\StudentController@update');
    Route::delete('/student/{student}', 'Student\StudentController@destroy');
    //Settings
    Route::get('/settings', 'Settings\SettingsController@index');
    Route::put('/settings/{setting}', 'Settings\SettingsController@update');
    //Family
    Route::get('/family', 'Family\FamilyController@index');
    Route::get('/family/{family}', 'Family\FamilyController@show');
    Route::put('/family/{family}', 'Family\FamilyController@update');
    //User companies
    Route::post('/registerCompany', 'UserCompany\UserCompanyController@store');
    Route::get('/userCompany', 'UserCompany\UserCompanyController@index');
    Route::get('/userCompany/{userCompany}', 'UserCompany\UserCompanyController@show');
    Route::put('/userCompany/{userCompany}', 'UserCompany\UserCompanyController@update');
    Route::delete('/userCompany/{userCompany}', 'UserCompany\UserCompanyController@destroy');
    //External promissory notes
    Route::get('/externalPromissoryNotes', 'ExternalPromissoryNote\ExternalPromissoryNoteController@index');
    //User family
    Route::get('/userFamily', 'UserFamily\UserFamilyController@index');
    Route::get('/userFamily/{userFamily}', 'UserFamily\UserFamilyController@show');
    Route::put('/userFamily/{userFamily}', 'UserFamily\UserFamilyController@update');
    Route::put('/userFamilyPassword/{family}', 'UserFamily\UserFamilyController@password');
    // UP Students
    Route::get('/upStudent', 'UpStudent\UpStudentController@index');
    Route::get('/upStudent/{upStudent}', 'UpStudent\UpStudentController@show');
    Route::put('/upStudent/{upStudent}', 'UpStudent\UpStudentController@update');
    Route::delete('/upStudent/{upStudent}', 'UpStudent\UpStudentController@destroy');
    // Finished requests
    Route::get('/finishedRequest/{campus}', 'FormVerification\FormVerificationController@finishedRequests');
    Route::delete('/finishedRequest/{formVerification}', 'FormVerification\FormVerificationController@destroy');
    // Started requests
    Route::get('/startedRequest', 'FormVerification\FormVerificationController@startedRequests');
    Route::put('/startedRequest/{familyId}', 'FormVerification\FormVerificationController@startedRequests');
    Route::delete('/startedRequest/{formVerification}', 'FormVerification\FormVerificationController@destroy');

    //Access family
    Route::get('/accessFamily/{campus}', 'UserFamily\UserFamilyController@accessOfFamilies');
    //Password reset
    Route::post('/passwordReset', 'Auth\UserController@showByUserId');
    Route::put('/passwordResetUpdate/{user}', 'Auth\UserController@updatePassword');
    //Families for research
    Route::get('/familiesForResearch', 'FormVerification\FormVerificationController@familiesForResearch');
    //Companies for families for research
    Route::put('/familiesForResearch/{family}', 'Family\FamilyController@updateCompanyId');
});

Route::group(['prefix' => 'general'], function () {
    // UP Students
    Route::get('/upStudent/{upStudent}', 'UpStudent\UpStudentController@show');
    Route::put('/upStudent/{upStudent}', 'UpStudent\UpStudentController@updateUpStatus');
    // Campus
    Route::get('/campus', 'GeneralResources\Campus\CampusController@index');
    Route::get('/campus/{campus}', 'GeneralResources\Campus\CampusController@show');
    // UP Status
    Route::get('/upStatus', 'GeneralResources\UpStatus\UpStatusController@index');
    Route::get('/upStatus/{upStatus}', 'GeneralResources\UpStatus\UpStatusController@show');
    // UP Career
    Route::get('/upCareer', 'GeneralResources\UpCareer\UpCareerController@index');
    Route::get('/upCareer/{upCareer}', 'GeneralResources\UpCareer\UpCareerController@show');
    // Business activity
    Route::get('/businessActivity', 'GeneralResources\BusinessActivity\BusinessActivityController@index');
    Route::get('/businessActivity/{businessActivity}', 'GeneralResources\BusinessActivity\BusinessActivityController@show');
    // Schoolarships
    Route::get('/schoolarship', 'GeneralResources\Schoolarship\SchoolarshipController@index');
    Route::get('/schoolarship/{schoolarship}', 'GeneralResources\Schoolarship\SchoolarshipController@show');
    // Professions
    Route::get('/profession', 'GeneralResources\Profession\ProfessionController@index');
    Route::get('/profession/{profession}', 'GeneralResources\Profession\ProfessionController@show');

});
Route::group(['prefix' => 'family'], function () {
    //User family
    Route::post('/loginFamily', 'UserFamily\UserFamilyController@authenticate');
    Route::put('/termsAndConditions/{family}', 'UserFamily\UserFamilyController@updateTermsAndConditions');
    //Password reset family
    Route::post('/passwordReset', 'PasswordResetUserFamilyMail\PasswordResetUserFamilyMailController@confirmation');
    Route::post('/passwordResetMail', 'PasswordResetUserFamilyMail\PasswordResetUserFamilyMailController@sendPasswordResetMail');

    // UP Students
    Route::post('/upStudent', 'UpStudent\UpStudentController@store');
    Route::get('/upStudent/{family}', 'UpStudent\UpStudentController@showByFamily');
    Route::put('/upStudent/{upStudent}', 'UpStudent\UpStudentController@updateSchoolarshipPercent');
    // Form verification
    Route::get('/formVerification', 'FormVerification\FormVerificationController@index');
    Route::get('/formVerification/{familyId}', 'FormVerification\FormVerificationController@show');
    Route::post('/formVerification', 'FormVerification\FormVerificationController@store');
    Route::put('/formVerification/{familyId}', 'FormVerification\FormVerificationController@update');

    // Heritage assets
    Route::get('/heritageAssets', 'HeritageAssets\HeritageAssetsController@index');
    Route::get('/heritageAssets/{family}', 'HeritageAssets\HeritageAssetsController@show');
    Route::post('/heritageAssets', 'HeritageAssets\HeritageAssetsController@store');
    Route::put('/heritageAssets/{family}', 'HeritageAssets\HeritageAssetsController@update');
    Route::delete('/heritageAssets/{heritageAssets}', 'HeritageAssets\HeritageAssetsController@destroy');
    // Monthly income
    Route::get('/monthlyIncome', 'MonthlyIncome\MonthlyIncomeController@index');
    Route::get('/monthlyIncome/{family}', 'MonthlyIncome\MonthlyIncomeController@show');
    Route::post('/monthlyIncome', 'MonthlyIncome\MonthlyIncomeController@store');
    Route::put('/monthlyIncome/{family}', 'MonthlyIncome\MonthlyIncomeController@update');
    Route::delete('/monthlyIncome/{monthlyIncome}', 'MonthlyIncome\MonthlyIncomeController@destroy');
    // Vehicles
    Route::get('/vehicle', 'Vehicle\VehicleController@index');
    Route::get('/vehicle/{family}', 'Vehicle\VehicleController@show');
    Route::post('/vehicle', 'Vehicle\VehicleController@store');
    Route::put('/vehicle/{family}', 'Vehicle\VehicleController@update');
    Route::delete('/vehicle/{vehicle}', 'Vehicle\VehicleController@destroy');
    // Economic dependent
    Route::get('/economicDependent', 'EconomicDependent\EconomicDependentController@index');
    Route::get('/economicDependent/{family}', 'EconomicDependent\EconomicDependentController@show');
    Route::post('/economicDependent', 'EconomicDependent\EconomicDependentController@store');
    Route::put('/economicDependent/{family}', 'EconomicDependent\EconomicDependentController@update');
    Route::delete('/economicDependent/{economicDependent}', 'EconomicDependent\EconomicDependentController@destroy');
    // Schoolarship Reason
    Route::get('/reason', 'SchoolarshipReason\SchoolarshipReasonController@index');
    Route::get('/reason/{family}', 'SchoolarshipReason\SchoolarshipReasonController@show');
    Route::post('/reason', 'SchoolarshipReason\SchoolarshipReasonController@store');
    Route::put('/reason/{family}', 'SchoolarshipReason\SchoolarshipReasonController@update');
    Route::delete('/reason/{schoolarshipReason}', 'SchoolarshipReason\SchoolarshipReasonController@destroy');
    //Student (schoolarship percent)
    Route::get('/studentSchoolarship/{family}', 'Student\StudentController@showStudentOfFamily');
    Route::put('/studentSchoolarship/{student}', 'Student\StudentController@updateSchoolarshipPercent');
    //Family information
    Route::get('/information/{family}', 'Family\FamilyController@showFamiyByFamilyId');
    Route::put('/information/{family}', 'Family\FamilyController@updateFamilyByFamilyId');
    // Monthly expense
    Route::get('/monthlyExpense', 'MonthlyExpense\MonthlyExpenseController@index');
    Route::get('/monthlyExpense/{family}', 'MonthlyExpense\MonthlyExpenseController@show');
    Route::post('/monthlyExpense', 'MonthlyExpense\MonthlyExpenseController@store');
    Route::put('/monthlyExpense/{family}', 'MonthlyExpense\MonthlyExpenseController@update');
    Route::delete('/monthlyExpense/{monthlyExpense}', 'MonthlyExpense\MonthlyExpenseController@destroy');

});
Route::group(['prefix' => 'research'], function () {
    //User companies
    Route::post('/loginCompany', 'UserCompany\UserCompanyController@authenticate');
    // Research form verification
    Route::put('/researchFormVerification/{familyId}', 'Research\ResearchFormVerification\ResearchFormVerificationController@update');
    Route::get('/formVerification', 'Research\ResearchFormVerification\ResearchFormVerificationController@index');
    Route::get('/formVerification/{familyId}', 'Research\ResearchFormVerification\ResearchFormVerificationController@show');
    Route::post('/formVerification', 'Research\ResearchFormVerification\ResearchFormVerificationController@store');
    Route::delete('/formVerification/{formVerification}', 'Research\ResearchFormVerification\ResearchFormVerificationController@destroy');
    Route::put('/updateCompany/{familyId}', 'Research\ResearchFormVerification\ResearchFormVerificationController@updateCompanyId');
    //Finished Researchs
    //By Campus
    Route::get('/formVerification/campus/{campus}', 'Research\ResearchFormVerification\ResearchFormVerificationController@finishedResearchsByCampus');
    //By Company
    Route::get('/formVerification/company/{company}', 'Research\ResearchFormVerification\ResearchFormVerificationController@finishedResearchsByCompany');
    //To Do Researchs
    Route::get('/formVerification/toDo/{company}', 'Research\ResearchFormVerification\ResearchFormVerificationController@toDoResearchs');

    // Family health
    Route::get('/familyHealth', 'Research\ResearchFamilyHealth\ResearchFamilyHealthController@index');
    Route::get('/familyHealth/{familyId}', 'Research\ResearchFamilyHealth\ResearchFamilyHealthController@show');
    Route::post('/familyHealth', 'Research\ResearchFamilyHealth\ResearchFamilyHealthController@store');
    Route::put('/familyHealth/{familyId}', 'Research\ResearchFamilyHealth\ResearchFamilyHealthController@update');
    Route::delete('/familyHealth/{familyHealth}', 'Research\ResearchFamilyHealth\ResearchFamilyHealthController@destroy');
    // Leisure time
    Route::get('/leisureTime', 'Research\ResearchLeisureTime\ResearchLeisureTimeController@index');
    Route::get('/leisureTime/{familyId}', 'Research\ResearchLeisureTime\ResearchLeisureTimeController@show');
    Route::post('/leisureTime', 'Research\ResearchLeisureTime\ResearchLeisureTimeController@store');
    Route::put('/leisureTime/{familyId}', 'Research\ResearchLeisureTime\ResearchLeisureTimeController@update');
    Route::delete('/leisureTime/{familyHealth}', 'Research\ResearchLeisureTime\ResearchLeisureTimeController@destroy');
    // Heritage assets detail
    Route::get('/heritageAssetsDetail', 'Research\ResearchHeritageAssetsDetail\ResearchHeritageAssetsDetailController@index');
    Route::get('/heritageAssetsDetail/{familyId}', 'Research\ResearchHeritageAssetsDetail\ResearchHeritageAssetsDetailController@show');
    Route::post('/heritageAssetsDetail', 'Research\ResearchHeritageAssetsDetail\ResearchHeritageAssetsDetailController@store');
    Route::put('/heritageAssetsDetail/{familyId}', 'Research\ResearchHeritageAssetsDetail\ResearchHeritageAssetsDetailController@update');
    Route::delete('/heritageAssetsDetail/{researchHeritageAssetsDetail}', 'Research\ResearchHeritageAssetsDetail\ResearchHeritageAssetsDetailController@destroy');
    // Research observation
    Route::get('/observation', 'Research\ResearchObservation\ResearchObservationController@index');
    Route::get('/observation/{familyId}', 'Research\ResearchObservation\ResearchObservationController@show');
    Route::post('/observation', 'Research\ResearchObservation\ResearchObservationController@store');
    Route::put('/observation/{familyId}', 'Research\ResearchObservation\ResearchObservationController@update');
    Route::delete('/observation/{researchFormVerification}', 'Research\ResearchObservation\ResearchObservationController@destroy');
    //Password reset
    Route::post('/passwordReset', 'UserCompany\UserCompanyController@showByUserId');
    Route::put('/passwordResetUpdate/{user}', 'UserCompany\UserCompanyController@updatePassword');
    // Cultural interest
    Route::get('/cultural', 'Research\ResearchCulturalInterest\ResearchCulturalInterestController@index');
    Route::get('/cultural/{family}', 'Research\ResearchCulturalInterest\ResearchCulturalInterestController@show');
    Route::post('/cultural', 'Research\ResearchCulturalInterest\ResearchCulturalInterestController@store');
    Route::put('/cultural/{family}', 'Research\ResearchCulturalInterest\ResearchCulturalInterestController@update');
    Route::delete('/cultural/{researchCulturalInterest}', 'Research\ResearchCulturalInterest\ResearchCulturalInterestController@destroy');
    // Reference
    Route::get('/reference/{family}', 'Research\ResearchReference\ResearchReferenceController@show');
    Route::get('/reference', 'Research\ResearchReference\ResearchReferenceController@index');
    Route::post('/reference', 'Research\ResearchReference\ResearchReferenceController@store');
    Route::put('/reference/{family}', 'Research\ResearchReference\ResearchReferenceController@update');
    Route::delete('/reference/{researchReference}', 'Research\ResearchReference\ResearchReferenceController@destroy');
    // Image
    Route::get('/image/{family}', 'Research\ResearchImage\ResearchImageController@show');
    Route::get('/image', 'Research\ResearchImage\ResearchImageController@index');
    Route::post('/image', 'Research\ResearchImage\ResearchImageController@store');
    Route::put('/image/{family}', 'Research\ResearchImage\ResearchImageController@update');
    Route::delete('/image/{researchImage}', 'Research\ResearchImage\ResearchImageController@destroy');
    // Situation and conclusion
    Route::get('/situationAndConclusion/{family}', 'Research\ResearchSituationAndConclusion\ResearchSituationAndConclusionController@show');
    Route::get('/situationAndConclusion', 'Research\ResearchSituationAndConclusion\ResearchSituationAndConclusionController@index');
    Route::post('/situationAndConclusion', 'Research\ResearchSituationAndConclusion\ResearchSituationAndConclusionController@store');
    Route::put('/situationAndConclusion/{family}', 'Research\ResearchSituationAndConclusion\ResearchSituationAndConclusionController@update');
    Route::delete('/situationAndConclusion/{researchSituationAndConclusion}', 'Research\ResearchSituationAndConclusion\ResearchSituationAndConclusionController@destroy');
});
Route::group(['prefix' => 'external'], function () {
    //Homologation and grades
    Route::get('/homologation', 'ExternalConsume\HomologationAndGrades\HomologationAndGradesController@index');
    //Decrypt and hashing passwords
    Route::get('/decrypt', 'ExternalConsume\PasswordDecryptAndHashing\PasswordDecryptAndHashingController@index');
});
// });
